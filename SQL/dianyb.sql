/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : dianyb

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2019-07-01 14:32:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_article
-- ----------------------------
DROP TABLE IF EXISTS `b_article`;
CREATE TABLE `b_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_title` varchar(50) NOT NULL DEFAULT '',
  `keyword` varchar(50) NOT NULL DEFAULT '',
  `describe` varchar(120) NOT NULL DEFAULT '',
  `art_content` text NOT NULL,
  `label` varchar(255) NOT NULL DEFAULT '',
  `cate_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '1直接发布 2保存草稿',
  `is_del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  `last_time` int(11) NOT NULL COMMENT '最后编辑时间',
  `release_time` int(11) NOT NULL COMMENT '发布时间',
  `author_id` int(11) NOT NULL COMMENT '作者',
  `forbid_comment` int(11) NOT NULL DEFAULT '1' COMMENT '禁止评论 0打开 1关闭',
  `top` int(11) NOT NULL DEFAULT '1' COMMENT '0 置顶 1不置顶',
  `browse_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_article
-- ----------------------------
INSERT INTO `b_article` VALUES ('1', '英超综合｜英超收官 曼城卫冕', '英超,曼城,利物浦,切尔西队,瓜迪奥拉', '曼城卫冕英超！神奇14连胜收官 蓝月王朝开始了！,英超 曼城 利物浦 切尔西队 瓜迪奥拉', '<div class=\"post_text\" id=\"endText\" style=\"border-top:1px solid #ddd;\">\r\n                                    <p>网易体育5月12日报道：</p><p>2018/19赛季<a href=\"/keywords/8/f/82f18d85/1.html\" title=\"英超\" target=\"_blank\">英超</a>落下帷幕，结局不出人所料 ，<a href=\"http://goal.sports.163.com/8/team/43.html\">曼城</a>以1分优势力压<a href=\"http://goal.sports.163.com/8/team/14.html\">利物浦</a>成功卫冕，拿到了队史第4座英超冠军，也是队史第6座顶级联赛桂冠。末轮毫无悬念击败布莱顿，蓝月军团以14连胜完美收官，刷新了英超历史收官连胜纪录。</p><p class=\"f_center\"><img alt=\"曼城卫冕英超！神奇14连胜收官 蓝月王朝开始了！\" src=\"http://cms-bucket.ws.126.net/2019/05/12/5776a88aa94b4a86afe6a27bba5247c2.jpeg?imageView&amp;thumbnail=550x0\"><br></p><p>上赛季，<a href=\"/keywords/6/f/66fc57ce/1.html\" title=\"曼城\" target=\"_blank\">曼城</a>在<a href=\"/keywords/7/d/74dc8fea596562c9/1.html\" title=\"瓜迪奥拉\" target=\"_blank\">瓜迪奥拉</a>的带领下，刷爆了单赛季联赛胜场次数、客场胜场次数、进球数以及净胜球数等英超N纪录，以创世纪的百分纪录夺冠，成就英超史上最强最高分冠军。本赛季，曼城长期遭遇伤病困扰，中场领袖德布劳内三番五次伤病缠身，比利时人频繁缺勤让球队大受影响。与此同时，在夏季转会市场上，蓝月军团也仅仅签下了马赫雷斯一名球员，反观他们竞争对手<a href=\"/keywords/5/2/522972696d66/1.html\" title=\"利物浦\" target=\"_blank\">利物浦</a>则是继续招兵买马，连续吃进了法比尼奥、沙奇里以及凯塔等悍将。当时，很多英国媒体预测曼城本赛季将遭遇滑坡，甚至可能重蹈<a href=\"http://goal.sports.163.com/8/team/8.html\">切尔西</a>之前的覆辙。</p><p>然而，令人感到出乎意料的是，瓜迪奥拉的球队在赛季开始便上紧发条，即便德布劳内因伤长期缺阵，蓝月军团在英超中依旧势不可挡。在进攻端上，由阿圭罗、斯特林、萨内、贝尔纳多-席尔瓦等球星组成的前场攻击群，水银泻地般的进攻更是让人赏音悦目，38场比赛下来狂轰94粒进球，期间一度6球血洗切尔西；在防守端上，由沃克、拉波尔特、奥塔门迪等人组成的后防线同样无懈可击，38轮比赛过后仅丢了23球。</p><p>神挡杀神，佛挡杀佛。虽然未能复制上赛季百分夺冠的傲人纪录，但曼城卫冕冠军的含金量绝不亚于上赛季，要知道他们最终拿到了98个积分，其中胜场次数达到了32球，这也追平了上赛季创造的英超单赛季胜场次数最高纪录。值得一提的是，随着此役顺利拿下布莱顿，曼城豪取联赛14连胜，他们也因此超越2001/02赛季的<a href=\"http://goal.sports.163.com/8/team/3.html\">阿森纳</a>，创造英超收官连胜纪录。以1分优势力压利物浦成功卫冕冠军，曼城也成为近10年来首支成功卫冕英超的球队。此前，曼城曾在2007/08赛季、2008/09赛季连续两个赛季夺取英超冠军。</p><p>从赛季前的社区盾杯，再到英格兰联赛杯，进而到今日的英超，曼城如今已先后3次夺魁。再过一个星期后，他们还将与沃特福德争夺足总杯冠军，不出意外的话，瓜迪奥拉将率队实现四冠王霸业。纵观整个赛季，尽管最终再次饮恨欧冠赛场，但曼城在国内赛场的霸主地位却无法撼动。毫不夸张地说，无论是观赏性还是统治力，瓜迪奥拉的球队下赛季依旧有怒冲四冠王伟业的资本！</p><p><b>【欢迎搜索关注公众号“足球大会”：只做最有意思的</b><b>足球</b><b>原创】</b></p><p><!--StartFragment--><!--EndFragment--></p><p class=\"f_center\"><img alt=\"英超-阿圭罗萨内破门 曼城2-1送利物浦英超首败\" src=\"http://cms-bucket.nosdn.127.net/2019/01/04/4661e219fca94186a823c89358d523f7.jpeg?imageView&amp;thumbnail=550x0\" style=\"vertical-align: top; border: 0px; margin: 0px auto; display: block;\"></p><p><!-- AD200x300_1 -->\r\n</p><div class=\"gg200x300\">\r\n<div class=\"at_item right_ad_item\" adtype=\"rightAd\" requesturl=\"https://nex.163.com/q?app=7BE0FC82&amp;c=sports&amp;l=133&amp;site=netease&amp;affiliate=sports&amp;cat=article&amp;type=logo300x250&amp;location=12\"><iframe width=\"300\" height=\"250\" frameborder=\"0\" border=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\"></iframe></div>\r\n<a href=\"javascript:;\" target=\"_self\" class=\"ad_hover_href\"></a>\r\n</div>\r\n                <p></p>\r\n                                                <div class=\"special_tag_wrap\"><div class=\"special_tag\" style=\"padding:20px\">网易足球俱乐部招募球迷啦！<br>球星周边，福利多多！资深球迷，等你来撩！<br>Q群：97061912。</div></div><div class=\"ep-source cDGray\">\r\n                    <span class=\"left\"><a href=\"http://sports.163.com/\"><img src=\"http://static.ws.126.net/cnews/css13/img/end_sports.png\" alt=\"刘派\" width=\"13\" height=\"12\" class=\"icon\"></a> 本文来源：网易体育  作者：辛夷</span>\r\n                    <!--刘派_NS6023--><span class=\"ep-editor\">责任编辑：刘派_NS6023</span>                </div>\r\n                                            </div>', '1,2,3', '1', '1', '1', '0', '0', '1', '1', '1', '0');
INSERT INTO `b_article` VALUES ('2', '张云雷用相声调侃汶川地震，凌晨致歉', '英超,曼城,利物浦,切尔西队,瓜迪奥拉', '曼城卫冕英超！神奇14连胜收官 蓝月王朝开始了！,英超 曼城 利物浦 切尔西队 瓜迪奥拉', '<div class=\"post_text\" id=\"endText\" style=\"border-top:1px solid #ddd;\">\r\n                                    <p>网易体育5月12日报道：</p><p>2018/19赛季<a href=\"/keywords/8/f/82f18d85/1.html\" title=\"英超\" target=\"_blank\">英超</a>落下帷幕，结局不出人所料 ，<a href=\"http://goal.sports.163.com/8/team/43.html\">曼城</a>以1分优势力压<a href=\"http://goal.sports.163.com/8/team/14.html\">利物浦</a>成功卫冕，拿到了队史第4座英超冠军，也是队史第6座顶级联赛桂冠。末轮毫无悬念击败布莱顿，蓝月军团以14连胜完美收官，刷新了英超历史收官连胜纪录。</p><p class=\"f_center\"><img alt=\"曼城卫冕英超！神奇14连胜收官 蓝月王朝开始了！\" src=\"http://cms-bucket.ws.126.net/2019/05/12/5776a88aa94b4a86afe6a27bba5247c2.jpeg?imageView&amp;thumbnail=550x0\"><br></p><p>上赛季，<a href=\"/keywords/6/f/66fc57ce/1.html\" title=\"曼城\" target=\"_blank\">曼城</a>在<a href=\"/keywords/7/d/74dc8fea596562c9/1.html\" title=\"瓜迪奥拉\" target=\"_blank\">瓜迪奥拉</a>的带领下，刷爆了单赛季联赛胜场次数、客场胜场次数、进球数以及净胜球数等英超N纪录，以创世纪的百分纪录夺冠，成就英超史上最强最高分冠军。本赛季，曼城长期遭遇伤病困扰，中场领袖德布劳内三番五次伤病缠身，比利时人频繁缺勤让球队大受影响。与此同时，在夏季转会市场上，蓝月军团也仅仅签下了马赫雷斯一名球员，反观他们竞争对手<a href=\"/keywords/5/2/522972696d66/1.html\" title=\"利物浦\" target=\"_blank\">利物浦</a>则是继续招兵买马，连续吃进了法比尼奥、沙奇里以及凯塔等悍将。当时，很多英国媒体预测曼城本赛季将遭遇滑坡，甚至可能重蹈<a href=\"http://goal.sports.163.com/8/team/8.html\">切尔西</a>之前的覆辙。</p><p>然而，令人感到出乎意料的是，瓜迪奥拉的球队在赛季开始便上紧发条，即便德布劳内因伤长期缺阵，蓝月军团在英超中依旧势不可挡。在进攻端上，由阿圭罗、斯特林、萨内、贝尔纳多-席尔瓦等球星组成的前场攻击群，水银泻地般的进攻更是让人赏音悦目，38场比赛下来狂轰94粒进球，期间一度6球血洗切尔西；在防守端上，由沃克、拉波尔特、奥塔门迪等人组成的后防线同样无懈可击，38轮比赛过后仅丢了23球。</p><p>神挡杀神，佛挡杀佛。虽然未能复制上赛季百分夺冠的傲人纪录，但曼城卫冕冠军的含金量绝不亚于上赛季，要知道他们最终拿到了98个积分，其中胜场次数达到了32球，这也追平了上赛季创造的英超单赛季胜场次数最高纪录。值得一提的是，随着此役顺利拿下布莱顿，曼城豪取联赛14连胜，他们也因此超越2001/02赛季的<a href=\"http://goal.sports.163.com/8/team/3.html\">阿森纳</a>，创造英超收官连胜纪录。以1分优势力压利物浦成功卫冕冠军，曼城也成为近10年来首支成功卫冕英超的球队。此前，曼城曾在2007/08赛季、2008/09赛季连续两个赛季夺取英超冠军。</p><p>从赛季前的社区盾杯，再到英格兰联赛杯，进而到今日的英超，曼城如今已先后3次夺魁。再过一个星期后，他们还将与沃特福德争夺足总杯冠军，不出意外的话，瓜迪奥拉将率队实现四冠王霸业。纵观整个赛季，尽管最终再次饮恨欧冠赛场，但曼城在国内赛场的霸主地位却无法撼动。毫不夸张地说，无论是观赏性还是统治力，瓜迪奥拉的球队下赛季依旧有怒冲四冠王伟业的资本！</p><p><b>【欢迎搜索关注公众号“足球大会”：只做最有意思的</b><b>足球</b><b>原创】</b></p><p><!--StartFragment--><!--EndFragment--></p><p class=\"f_center\"><img alt=\"英超-阿圭罗萨内破门 曼城2-1送利物浦英超首败\" src=\"http://cms-bucket.nosdn.127.net/2019/01/04/4661e219fca94186a823c89358d523f7.jpeg?imageView&amp;thumbnail=550x0\" style=\"vertical-align: top; border: 0px; margin: 0px auto; display: block;\"></p><p><!-- AD200x300_1 -->\r\n</p><div class=\"gg200x300\">\r\n<div class=\"at_item right_ad_item\" adtype=\"rightAd\" requesturl=\"https://nex.163.com/q?app=7BE0FC82&amp;c=sports&amp;l=133&amp;site=netease&amp;affiliate=sports&amp;cat=article&amp;type=logo300x250&amp;location=12\"><iframe width=\"300\" height=\"250\" frameborder=\"0\" border=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\"></iframe></div>\r\n<a href=\"javascript:;\" target=\"_self\" class=\"ad_hover_href\"></a>\r\n</div>\r\n                <p></p>\r\n                                                <div class=\"special_tag_wrap\"><div class=\"special_tag\" style=\"padding:20px\">网易足球俱乐部招募球迷啦！<br>球星周边，福利多多！资深球迷，等你来撩！<br>Q群：97061912。</div></div><div class=\"ep-source cDGray\">\r\n                    <span class=\"left\"><a href=\"http://sports.163.com/\"><img src=\"http://static.ws.126.net/cnews/css13/img/end_sports.png\" alt=\"刘派\" width=\"13\" height=\"12\" class=\"icon\"></a> 本文来源：网易体育  作者：辛夷</span>\r\n                    <!--刘派_NS6023--><span class=\"ep-editor\">责任编辑：刘派_NS6023</span>                </div>\r\n                                            </div>', '1,2,3', '1', '1', '1', '0', '0', '1', '1', '1', '0');
INSERT INTO `b_article` VALUES ('3', '213123213213213213123123', '213123213213213213123123', '213123213213213213123123', '<p>213123213213213213123123</p>', '213123213213213213123123', '1', '1', '1', '1557995350', '1557995350', '1', '0', '0', '0');
INSERT INTO `b_article` VALUES ('4', '21312321321321321312312321212', '213123213213213213123123', '213123213213213213123123', '<p>213123213213213213123123</p>', '体育新闻', '1', '1', '1', '1557995513', '1557995513', '1', '0', '1', '0');
INSERT INTO `b_article` VALUES ('5', '解决微信小程序input、textarea层级过高穿透问题11', '解决微信小程序input、textarea层级过高穿透问题', '解决微信小程序input、textarea层级过高穿透问题', '<p>微信小程序原生组件camera、canvas、input（仅在focus时表现为原生组件）、live-player、live、pusher、map、textarea、video的层级是最高的，页面中的其他组件无论设置 z-index 为多少，都无法盖在原生组件上。</p><p><br/></p><p>从图中效果可以看出input输入的内容穿透到了选择列表自定义组件，解决办法：</p><p>  1、使用if，当触发选择列表时隐藏input或textare组件，选择完成点击确认关闭选择列表，显示input或textare。</p><p>  2、选择列表自定义组件使用 cover-view 和 cover-image 组件代替view和image，cover-view 和 cover-image 组件，可以覆盖在部分原生组件上面。</p><p><br/></p><p><br/></p>', '体育新闻,亚洲咨询', '1', '1', '0', '1558146831', '1557996218', '1', '0', '1', '0');
INSERT INTO `b_article` VALUES ('6', '英超综合132123', '英超综合132123', '英超综合132123', '<p>英超综合132123</p>', '体育新闻', '1', '1', '0', '1558148111', '1558148111', '1', '0', '1', '0');
INSERT INTO `b_article` VALUES ('7', '解决微信小程序input、textarea层级过高穿透问题231321', '解决微信小程序input、textarea层级过高穿透问题231321', '解决微信小程序input、textarea层级过高穿透问题231321', '<p>解决微信小程序input、textarea层级过高穿透问题231321</p>', '体育新闻', '1', '1', '0', '1558148003', '1558148003', '1', '0', '1', '0');
INSERT INTO `b_article` VALUES ('8', '英超综合12312213213', '英超综合12312213213', '英超综合12312213213', '<p>英超综合12312213213</p>', '体育新闻,亚洲咨询', '1', '1', '1', '1558148180', '0', '1', '0', '1', '0');
INSERT INTO `b_article` VALUES ('9', '2131232132132132131231232123', '2131232132132132131231232123', '2131232132132132131231232123', '<p>2131232132132132131231232123</p>', '体育新闻,亚洲咨询', '1', '1', '0', '1558148521', '1558148521', '1', '0', '1', '333');
INSERT INTO `b_article` VALUES ('10', '解决微信小程序input、textarea层级过高穿透问题666', '解决微信小程序input、textarea层级过高穿透问题666', '解决微信小程序input、textarea层级过高穿透问题666', '<p>解决微信小程序input、textarea层级过高穿透问题666</p>', '体育新闻,亚洲咨询', '1', '1', '0', '1558148569', '1558148569', '1', '0', '0', '666');
INSERT INTO `b_article` VALUES ('11', '特朗普特朗普特朗普特朗普特朗普特朗普特朗普特朗普特朗普特朗普', '特朗普特朗普特朗普特朗普特朗普特朗普特朗普特朗普', '特朗普特朗普特朗普特朗普', '<p><img src=\"/uploads/image/20190524/1558671655951346.jpg\" title=\"1558671655951346.jpg\" alt=\"1.jpg\"/></p>', '体育新闻,亚洲咨询', '1', '1', '0', '1558671724', '1558671724', '1', '0', '1', '139361');
INSERT INTO `b_article` VALUES ('12', 'Ueditor路径配置（上传图片及视频）', 'Ueditor路径配置（上传图片及视频）', 'Ueditor路径配置（上传图片及视频）', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 25px; color: rgb(47, 47, 47); font-family: -apple-system, \" sf=\"\" ui=\"\" pingfang=\"\" hiragino=\"\" sans=\"\" microsoft=\"\" wenquanyi=\"\" micro=\"\" white-space:=\"\" background-color:=\"\" word-break:=\"\" break-word=\"\">由于Ueditor编辑器的图片上传后，默认存储位置是在www根目录下，而不是在网站项目目录下，因此需要重新配置。由于网站项目名有多个，因此需要动态与存储地址进行拼接。（等不及的可以直接拉到最后看总结的步骤）</p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 25px; color: rgb(47, 47, 47); font-family: -apple-system, \" sf=\"\" ui=\"\" pingfang=\"\" hiragino=\"\" sans=\"\" microsoft=\"\" wenquanyi=\"\" micro=\"\" white-space:=\"\" background-color:=\"\" word-break:=\"\" break-word=\"\"><span style=\"box-sizing: border-box; font-weight: 700;\">config.json</span></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 25px; color: rgb(47, 47, 47); font-family: -apple-system, \" sf=\"\" ui=\"\" pingfang=\"\" hiragino=\"\" sans=\"\" microsoft=\"\" wenquanyi=\"\" micro=\"\" white-space:=\"\" background-color:=\"\" word-break:=\"\" break-word=\"\"><span style=\"box-sizing: border-box; font-weight: 700;\">修改上传保存路径（自定义路径）</span></p><p style=\"text-align: center;\"><img src=\"/uploads/image/20190530/1559207208569439.png\" title=\"1559207208569439.png\" alt=\"fc02dc78713847c5837c46abecccd1db.png\"/></p>', '体育新闻', '1', '1', '0', '1559207238', '1559207216', '1', '0', '1', '0');

-- ----------------------------
-- Table structure for b_cate
-- ----------------------------
DROP TABLE IF EXISTS `b_cate`;
CREATE TABLE `b_cate` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(20) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL,
  `level` int(1) NOT NULL DEFAULT '0',
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`cate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_cate
-- ----------------------------
INSERT INTO `b_cate` VALUES ('1', '新闻', '0', '1', '0');
INSERT INTO `b_cate` VALUES ('2', '中国新闻2', '1', '2', '1');
INSERT INTO `b_cate` VALUES ('3', '国际', '0', '1', '1');
INSERT INTO `b_cate` VALUES ('4', '亚洲新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('5', '非洲新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('6', '美洲新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('7', '体育新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('8', '财经新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('9', '足球新闻', '1', '2', '0');
INSERT INTO `b_cate` VALUES ('10', '篮球新闻', '1', '2', '0');

-- ----------------------------
-- Table structure for b_collect
-- ----------------------------
DROP TABLE IF EXISTS `b_collect`;
CREATE TABLE `b_collect` (
  `collect_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `add_time` int(11) NOT NULL DEFAULT '0',
  `is_del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`collect_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_collect
-- ----------------------------

-- ----------------------------
-- Table structure for b_comment
-- ----------------------------
DROP TABLE IF EXISTS `b_comment`;
CREATE TABLE `b_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `comment_user` varchar(30) NOT NULL DEFAULT '',
  `comment` varchar(255) NOT NULL DEFAULT '',
  `reply` varchar(255) NOT NULL DEFAULT '',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `comment_time` int(11) NOT NULL,
  `is_show` int(11) NOT NULL DEFAULT '0' COMMENT '0显示 1不显示',
  `del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_comment
-- ----------------------------
INSERT INTO `b_comment` VALUES ('1', '11', '张三', '盛世英超，谁能不爱，将产生欧冠冠亚军欧联冠亚军超级杯冠亚军，史上最强亚军，史上最具含金量第六名，3位金靴，下季见拜拜———————詹俊盛世英超，谁能不爱，将产生欧冠冠亚军欧联冠亚军超级杯冠亚军，史上最强亚军，史上最具含金量第六名，3位金靴，下季见拜拜———————詹俊\r\n盛世英超，谁能不爱，将产生欧冠冠亚军欧联冠亚军超级杯冠亚军，史上最强亚军，史上最具含金量第六名，3位金靴，下季见拜拜———————詹俊盛世英超，谁能不爱，将产生欧冠冠亚军欧联冠亚军超级杯冠亚军，史上最强亚军，史上最具———————詹俊', '谢谢', '1', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('2', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('3', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('4', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('5', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('6', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('7', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('8', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('9', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('10', '11', '张三', '利物浦就是太贪心了，自己什么板凳深度不知道吗？早点放弃欧冠，认真打联赛多好。英超从未夺过冠，欧冠又不是没拿过', '', '0', '0', '0', '0');
INSERT INTO `b_comment` VALUES ('11', '0', '李三', '胜多负少的范德萨', '', '0', '1558927800', '0', '0');
INSERT INTO `b_comment` VALUES ('12', '11', '李三', '适当方式的方式', '', '0', '1558928008', '0', '0');
INSERT INTO `b_comment` VALUES ('13', '11', '李三', '水电费水电费第三方第三方胜多负少的', '', '0', '1558928344', '0', '0');
INSERT INTO `b_comment` VALUES ('14', '11', '李三', '水电费水电费第三方第三方胜多负少的', '', '0', '1558928358', '0', '0');
INSERT INTO `b_comment` VALUES ('15', '11', '李三', '胜多负少的开始对方考虑是的教室里的快递客服解释道', '', '0', '1558928411', '0', '0');
INSERT INTO `b_comment` VALUES ('16', '11', '脸书', '水电费水电费', '', '0', '1558928513', '0', '0');
INSERT INTO `b_comment` VALUES ('17', '11', '来说', '水电费水电费', '', '0', '1558928591', '0', '0');
INSERT INTO `b_comment` VALUES ('18', '11', '脸书', '水电费水电费', '', '0', '1558928686', '0', '0');

-- ----------------------------
-- Table structure for b_feedback
-- ----------------------------
DROP TABLE IF EXISTS `b_feedback`;
CREATE TABLE `b_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `add_time` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_feedback
-- ----------------------------
INSERT INTO `b_feedback` VALUES ('1', '321321313', '1561541651', '1');
INSERT INTO `b_feedback` VALUES ('2', '88888888888888888888888888888888', '1561541712', '1');

-- ----------------------------
-- Table structure for b_group
-- ----------------------------
DROP TABLE IF EXISTS `b_group`;
CREATE TABLE `b_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `auth` text COMMENT '权限菜单',
  `remark` text COMMENT '备注',
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`group_id`),
  KEY `id` (`group_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_group
-- ----------------------------
INSERT INTO `b_group` VALUES ('1', '超级管理员', '1,2,36,37,38,39,4,30,31,32,5,27,28,29,6,34,35,40,41,42,43,44,45,46,47,48,49,50,51,52,8,9,20,21,22,23,24,25,26,10,11,33', '超级管理员，拥有最高权限！', '0');
INSERT INTO `b_group` VALUES ('20', '文字编辑', '1,4,5,6,7,3,2,8,23,24,25,26,10,11,12', '我是备注', '0');
INSERT INTO `b_group` VALUES ('21', '测试分组', '1,4,3,9,10', '666', '1');

-- ----------------------------
-- Table structure for b_history
-- ----------------------------
DROP TABLE IF EXISTS `b_history`;
CREATE TABLE `b_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `add_time` int(11) NOT NULL DEFAULT '0',
  `is_del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_history
-- ----------------------------
INSERT INTO `b_history` VALUES ('3', '1', '51', '1561892832', '0');

-- ----------------------------
-- Table structure for b_label
-- ----------------------------
DROP TABLE IF EXISTS `b_label`;
CREATE TABLE `b_label` (
  `label_id` int(11) NOT NULL AUTO_INCREMENT,
  `label_name` varchar(20) NOT NULL DEFAULT '',
  `is_del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`label_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_label
-- ----------------------------
INSERT INTO `b_label` VALUES ('1', '体育新闻', '0');
INSERT INTO `b_label` VALUES ('2', '亚洲咨询', '0');
INSERT INTO `b_label` VALUES ('3', '开飞机2', '1');

-- ----------------------------
-- Table structure for b_log
-- ----------------------------
DROP TABLE IF EXISTS `b_log`;
CREATE TABLE `b_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `log_value` text NOT NULL,
  `manage_id` int(11) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_log
-- ----------------------------
INSERT INTO `b_log` VALUES ('2', 'login', '{\"time\":1558255511,\"ip\":\"::1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('3', 'login', '{\"time\":1558256402,\"ip\":\"::1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('4', 'login', '{\"time\":1558671628,\"ip\":\"::1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('5', 'login', '{\"time\":1559206929,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('6', 'login', '{\"time\":1559292592,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('7', 'login', '{\"time\":1559363404,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('8', 'login', '{\"time\":1559363483,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(70.0.3538.110)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('9', 'login', '{\"time\":1560600731,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(74.0.3729.169)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('10', 'login', '{\"time\":1560600787,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(74.0.3729.169)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('11', 'login', '{\"time\":1560601259,\"ip\":\"127.0.0.1\",\"broswer\":\"Firefox(67.0)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('12', 'login', '{\"time\":1561618454,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(75.0.3770.100)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('13', 'login', '{\"time\":1561618825,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(75.0.3770.100)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('14', 'login', '{\"time\":1561688847,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(75.0.3770.100)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('15', 'login', '{\"time\":1561767011,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(75.0.3770.100)\",\"os\":\"Windows 10\"}', '1');
INSERT INTO `b_log` VALUES ('16', 'login', '{\"time\":1561857882,\"ip\":\"127.0.0.1\",\"broswer\":\"Chrome(75.0.3770.100)\",\"os\":\"Windows 10\"}', '1');

-- ----------------------------
-- Table structure for b_lunbo
-- ----------------------------
DROP TABLE IF EXISTS `b_lunbo`;
CREATE TABLE `b_lunbo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) DEFAULT NULL,
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_lunbo
-- ----------------------------
INSERT INTO `b_lunbo` VALUES ('1', '/uploads/temp/1.jpg', null, '0');
INSERT INTO `b_lunbo` VALUES ('2', '/uploads/temp/2.jpg', null, '0');

-- ----------------------------
-- Table structure for b_manage
-- ----------------------------
DROP TABLE IF EXISTS `b_manage`;
CREATE TABLE `b_manage` (
  `manage_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `login_ip` varchar(20) NOT NULL DEFAULT '',
  `login_time` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `real_name` varchar(10) NOT NULL DEFAULT '',
  `id_card` varchar(18) NOT NULL DEFAULT '',
  `add_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_manage
-- ----------------------------
INSERT INTO `b_manage` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('2', 'admin1', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('3', 'admin3', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('4', 'admin4', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('5', 'admin5', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('6', 'admin6', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('7', 'admin7', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('8', 'admin8', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('9', 'admin9', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('10', 'admin10', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('12', 'admin11', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('13', 'admin12', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('14', 'admin13', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('15', 'admin15', 'e10adc3949ba59abbe56e057f20f883e', 'xxx@qq.com', '192.168.1.1', '0', '1', '李三', '211122198802260315', '0');
INSERT INTO `b_manage` VALUES ('18', 'admin999', 'e10adc3949ba59abbe56e057f20f883e', '123456@qq.com', '', '0', '1', '一级', '211122198802260315', '1557179225');
INSERT INTO `b_manage` VALUES ('19', 'admin10101', 'e10adc3949ba59abbe56e057f20f883e', '123456@qq.com', '', '0', '1', '一级', '211122198802260315', '1557179318');
INSERT INTO `b_manage` VALUES ('20', 'admin02121', 'e10adc3949ba59abbe56e057f20f883e', '123456@qq.com', '', '0', '1', '一级', '211122198802260315', '1557179485');

-- ----------------------------
-- Table structure for b_member
-- ----------------------------
DROP TABLE IF EXISTS `b_member`;
CREATE TABLE `b_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(18) NOT NULL DEFAULT '',
  `phone_num` varchar(11) NOT NULL,
  `password` varchar(32) NOT NULL DEFAULT '',
  `sex` int(1) NOT NULL DEFAULT '1' COMMENT '0女 1男',
  `add_time` int(11) NOT NULL DEFAULT '0',
  `token` varchar(32) NOT NULL DEFAULT '',
  `is_del` int(11) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_member
-- ----------------------------
INSERT INTO `b_member` VALUES ('1', '张三', '13604218613', '4297f44b13955235245b2497399d7a93', '1', '0', 'f1dcd775e0d6be477aba408c8ff2b7c5', '0');
INSERT INTO `b_member` VALUES ('3', '王五', '13604218611', 'e10adc3949ba59abbe56e057f20f883e', '1', '1561344615', '33d0b2833b14abb5990ef2aef82ce389', '0');

-- ----------------------------
-- Table structure for b_movie
-- ----------------------------
DROP TABLE IF EXISTS `b_movie`;
CREATE TABLE `b_movie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_title` varchar(30) NOT NULL DEFAULT '',
  `actor` varchar(50) DEFAULT NULL,
  `score` float(2,1) NOT NULL DEFAULT '1.0',
  `browse_num` int(11) NOT NULL DEFAULT '0',
  `intro` text NOT NULL,
  `face_pic` varchar(255) NOT NULL DEFAULT '',
  `movie_path` varchar(255) NOT NULL DEFAULT '',
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '0正常 1删除',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0 正常 1热门强推',
  `add_time` int(11) NOT NULL DEFAULT '0',
  `movie_cate_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_movie
-- ----------------------------
INSERT INTO `b_movie` VALUES ('51', '传染病', '玛丽昂·歌迪亚,马特·达蒙,劳伦斯·菲什伯恩,裘德·洛,格温妮斯·帕特', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">影片讲述了一种新型致命病毒在几天之内席卷全球的故事。贝丝（格温妮丝·帕特洛GwynethPaltrow饰）从香港出差回明尼阿波利斯的家后突发疾病死亡，丈夫托马斯（马特·达蒙MattDamon饰）协助Dr.ErinMears（凯特·温斯莱特KateWinslet饰）调查真相。可怕的是同样病症的患者在世界范围内大规模出现，病毒在不停蔓延。阴谋论记者阿伦（裘德·洛JudeLaw饰）趁机散布对政府和医疗机构不利的舆论。Dr.LeonoraOrantes（玛丽昂·歌迪亚MarionCotillard饰）被派往香港调查病因，但却被本土医生（黄经汉ChinHan饰）扣留作为交换疫苗的人质。在Dr.EllisCheever（劳伦斯·菲什伯恩LaurenceFishburne饰）和年轻医生AllyHext...</span></p>', '/uploads/image/20190630/e46d9fbf8f5d80832e0acbd67927e183.jpg', 'https://www.hbkdtz.com:987/20190622/EdoSNNu8/index.m3u8', '0', '1', '1561892325', '1');
INSERT INTO `b_movie` VALUES ('50', '不明飞行物', 'Bianca Bree,肖恩·布鲁斯南,西蒙·飞利普斯', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">老牌动作明星尚格·云顿在这部科幻新片中饰演一位退役军事顾问，不过他并非主角，影片主人公是五个年轻人，某日他们早上醒来发现生活陷入了一种说不出的诡异状态，在一阵剧烈的地震过后，天空出现了一个城市般大小的巨型UFO。整个城镇电力消失，手机也没了信号……人类面临庞大的外星军队的突袭！</span></p>', '/uploads/image/20190630/06f13e32340fa4ac6e93f1118e5cccfc.jpg', 'https://www.hbkdtz.com:987/20190622/GvjRvohm/index.m3u8', '0', '1', '1561892267', '1');
INSERT INTO `b_movie` VALUES ('34', '生化危机：诅咒', '马修·默瑟,戴夫·维滕博格,柯特奈·泰勒,ValTasso,罗宾·萨奇斯', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">1991年，苏联解体，原隶属苏联体系下的社会主义国家纷纷宣布独立，东斯拉夫共和国正是众多新生国家中的一员。然而东斯拉夫两极分化严重，社会严重失衡，反叛军与政府军进行了长达十数年的战争。时间进入21世纪，双方的战争愈演愈烈，甚至传闻已有“怪物”加入进来，战争开始朝着不确定的方向发展。为了调查生化武器的实情，里昂获命来到东斯拉夫。混乱之中，他被反叛军抓获，可是遭到寄生虫操控的人类很快冲击了他们的栖息地和城市。与此同时，艾达也抱着神秘目的来到东斯拉夫。战争毁灭了一切，滋养着恶魔，里昂能否阻止事态进一步恶化呢……</span></p>', '/uploads/image/20190630/ce20d71c116fb3781b11d7d16449d274.jpg', 'https://v3.mjshcn.com:987/20190629/sZn6c736/index.m3u8', '0', '1', '1561891876', '1');
INSERT INTO `b_movie` VALUES ('35', '星际旅行2：可汗之怒', '威廉·夏特纳,伦纳德·尼莫伊,德福雷斯特·凯利,詹姆斯·杜汉,沃尔特·', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">邪恶的可汗逃脱十五年前被寇克船长拘禁的星球，他本人是一位智勇双全的领袖，但此刻已变成一个邪恶的狂人，他誓言要向寇克船长讨回血仇。于是他先劫持了一艘联邦战舰，后又偷取了寇克船长的儿子和旧情人制造的毁灭性武器。并引诱企业号投入他精心设计的圈套……</span></p>', '/uploads/image/20190630/f7f7a9f6668461ce463ce8117bb57de8.jpg', 'https://v2.mjshcn.com:987/20190612/zvUKdeSM/index.m3u8', '0', '1', '1561891855', '1');
INSERT INTO `b_movie` VALUES ('36', '哈利·波特与死亡圣器[下]', '丹尼尔·雷德克里夫,艾玛·沃森,鲁伯特·格林特,海伦娜·伯翰·卡特,拉', '2.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">当又一次和伏地魔（拉尔夫·费因斯RalphFiennes饰）的意识连通，哈利·波特（丹尼尔·雷德克里夫DanielRadcliffe饰）断定最后一件魂器藏在霍格沃茨，于是和罗恩（鲁伯特·格林特RupertGrint饰）、赫敏（艾玛·沃森EmmaWatson饰）一同返回阴云密布的学校。在好友们的帮助下，他们成功驱逐了斯内普（艾伦·瑞克曼AlanRickman饰），然而觉察到哈利目的的伏地魔则率领徒众向霍格沃茨逼近。食死徒、摄魂怪、巨人疯狂涌入这所有着悠久历史的魔法学校，正邪决战旋即爆发，一时间血雨腥风，死伤无数。从斯内普的眼泪中，哈利不仅了解到父辈的故事，也证实了藏缅于他体内最后的秘密。在此之后，他也和伏地魔迎来了最后的对决……本片根据英国作家J.K.罗琳的同名原著改编，也是“哈利·波特”系列影片的完结篇。</span></p>', '/uploads/image/20190630/4d69424a2e942bc21ca23738e7561522.jpg', 'https://www.hbkdtz.com:987/20190622/BRA1F4Nq/index.m3u8', '0', '0', '1561890519', '1');
INSERT INTO `b_movie` VALUES ('37', '星际迷航2：暗黑无界', '克里斯·派恩,扎克瑞·昆图,佐伊·索尔达娜,本尼迪克特·康伯巴奇', '3.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">在尼比鲁星球探查期间，企业号舰长柯克（克里斯·派恩Chris Pine 饰）为营救史波克（扎克瑞·昆图 Zachary Quinto 饰）采取了胆大妄为的举动，几乎危及全舰队员的生命，他也为此付出代价。不久，柯克官复原职，而伦敦发生一起死亡超过四十人的恐怖袭击事件。该事件的始作俑者约翰·哈里森（本尼迪克特·康伯巴奇 Benedict Cumberbatch 饰）正来自联盟内部。此后他驾驶飞机袭击了高层会议，柯克最为敬重的派克将军不幸遇难。为了阻止哈里森的外逃阴谋，柯克重新率领企业号启程追击。危机重重的旅途，他和舰员、朋友之间的信任也受到巨大考验。当他们最终找到哈里森，才终于知晓他的真实身份，以及伦敦袭击事件背后的阴谋和黑幕……</span></p>', '/uploads/image/20190630/cd5ca098d181c1de18175d5077d66bf0.jpg', 'https://www.hbkdtz.com:987/20190622/MmaiU4Kr/index.m3u8', '0', '0', '1561890603', '1');
INSERT INTO `b_movie` VALUES ('38', '驱魔者', '保罗·贝坦尼,凯姆·吉甘戴,李美琪,卡尔·厄本,莉莉·柯林斯,克里斯托', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">长久以来，人类与吸血鬼的战斗从未停息。很多年之后，人类为了生存，寄居于教会严密控制的城市之中，并且训练了猎魔教士来保护自己。在结束了与吸血鬼的战争之后，猎魔教士的队伍被解散，并逐渐被世人所遗忘。曾在战时立下汗马功勋的传奇猎魔教士（保罗?贝坦尼PaulBettany饰）已隐匿多年，可是忽然造访的来自奥古斯丁的警长希克斯（凯姆?吉甘戴CamGigandet饰）却给他带来一个不好的消息，远在荒远的亲人受到吸血鬼的攻击，并且掳走了他的侄女露西。为了救出亲人，教士不惜违背主教们的意志，开始踏上自己的拯救之旅。此时，由于违背了主教的意志，主教派出四名猎魔教士对传奇教士展开追捕……本片改编自在美国广受好评的韩国漫画家邢民友的漫画作品《Priest》。</span></p>', '/uploads/image/20190630/f086680fc0003b8323161e95a7983d69.jpg', 'https://www.hbkdtz.com:987/20190622/VtxUtuag/index.m3u8', '0', '0', '1561890696', '1');
INSERT INTO `b_movie` VALUES ('39', '末日浩劫', '基姆·古铁雷斯,何塞·科罗纳多,玛尔塔·埃图拉,拉蒂西亚·多瑞拉', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">时为2013年，世界各地突然被死神的阴影所笼罩。凡是身处开放空间的人类，无一例外将丧失宝贵生命。外表俊朗的马克（奎姆·古铁雷兹 Quim Gutiérrez 饰）是一条生活在巴塞罗那的苦闷程序狗，上司埃里克（José Coronado 饰）不近人情的苛刻要求压得他早已喘不过气来，而与美丽女友朱莉娅（玛塔·埃图 娜 Marta Etura 饰）的关系似乎也进入了瓶颈期。灾难降临后，马克和其他人被困在某栋建筑物内长达三个月。担心女友的安全，他决定铤而走险离开这个绝地。马克与持有GPS的埃里克携手，穿越重重危险向外行进。繁华的大都市，人们却早已习惯自我囚禁在隔绝的空间内，而宁愿丧失与他人交流的能力。幡然醒悟，为时已晚……©豆瓣</span></p>', '/uploads/image/20190630/e3a28715d9b4c308f16be7f5e18fd2db.jpg', 'https://www.hbkdtz.com:987/20190622/n3KYqwB5/index.m3u8', '0', '0', '1561890769', '1');
INSERT INTO `b_movie` VALUES ('40', '诸神之怒', '萨姆·沃辛顿,拉尔夫·费因斯,连姆·尼森,丹尼·赫斯顿', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">结束了与北海巨妖的战争，英雄珀修斯（萨姆·沃辛顿 Sam Worthington 饰）与儿子赫利俄斯（约翰·贝尔 John Bell 饰）在一座小村庄过着平静的生活。但是奥林匹亚的战火从未停歇，以宙斯（连姆•尼森 Liam Neeson 饰）为首的众神与泰坦巨人进行着旷日持久的惨烈战争，甚至连被宙斯囚禁良久的三神（天神宙斯、海神波塞冬、冥王哈迪斯）之父——克洛诺斯也即将复活。战争的天平渐渐向泰坦巨人一方倾斜，在冥王哈迪斯（拉尔夫·费因斯 Ralph Fiennes 饰）的策划下，宙斯误入冥界，神力尽失，遭到囚禁。为了驱逐黑暗，将光明带回大地，珀修斯重披铠甲，与一众英雄前往冥界营救宙斯，诸神的战火迎来了最关键的时刻……本片为2010年3D电影《诸神之战》的续集。©豆瓣</span></p>', '/uploads/image/20190630/157d2b396bb2d34ba4e59e928b340c30.jpg', 'https://www.hbkdtz.com:987/20190622/edbNU2GF/index.m3u8', '0', '0', '1561890841', '1');
INSERT INTO `b_movie` VALUES ('41', '最后的勇士', '维克多·霍林雅克,米拉·斯瓦奇卡娅,叶卡婕琳娜·维尔科娃,康斯坦丁', '2.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">伊万是个混日子的屌丝，经常假装自己会魔法，虚张声势弄虚作假帮助客户解决问题以牟取暴利。某天他忽然去到一个可以让魔法成真的陌生地方。这个地方被称为贝罗里奥，这里居住着俄罗斯民间史诗里的人物。伊万很惊讶地认识了科西切、芭芭雅嘎、Vodaynoy和Vasilisa。由于某种原因，他们都确信伊万是伊里亚·穆罗梅茨失去的儿子，认为他是拯救贝罗里奥的人。在这个童话世界里，一切都是颠倒过来的：善良战胜邪恶都是曾经的事，并且一个个的战士开始消失，而伊万恰好是最后一位将世界恢复平衡秩序的战士……</span></p>', '/uploads/image/20190630/f4ddaa6cae94e069ef9827d97e21ebc5.jpg', 'https://v3.mjshcn.com:987/20190628/x16scuEp/index.m3u8', '0', '0', '1561890913', '1');
INSERT INTO `b_movie` VALUES ('42', '心情风暴', '汤姆·卡瓦纳夫 / 梅丽莎·吉尔伯特 / 马库斯·里尔·布朗 / 艾珠·帕森', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">在12级飓风快到达美国新奥尔良前，相关政府部门正在通过各方面的信号通知民众飓风的到来。Cassie（梅丽莎·吉尔伯特 饰）和丈夫韦恩（Brian Wimmer 饰）离婚后一直抚养两个女儿，她们住在遥远的河口支流处。这天夜幕降临，一个危险罪犯与两个同犯正从监狱里逃脱出来，飓风越来越强，它破坏了他们的逃命计划，他们不得不躲在Cassie的家里。Cassie起初以为他们只是躲避风雨的过客，知道真相后Cassie和女儿都处在恐惧中，她们不能依靠远在市区的韦恩，只能通过自己方式来解救自己。</span></p>', '/uploads/image/20190630/d66a277a008b61f9d958474ec4387cce.jpg', 'https://v3.mjshcn.com:987/20190629/oYv3u4HA/index.m3u8', '0', '1', '1561891788', '3');
INSERT INTO `b_movie` VALUES ('43', '天上的恋人', '刘烨,陶虹,董洁,冯恩鹤', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, &quot;hiragino sans gb&quot;, Helvetica, Arial; font-size: 12px; background-color: rgb(253, 253, 253);\">广西某个美如仙境的山村，聋子王家宽（刘烨）爱上村里最漂亮的姑娘朱灵（陶虹）后，开始用自己独特的方式对其展开追求，朱灵虽深受感动，心仍属了别人。从外地来寻找哥哥的蔡玉珍（董洁）暂住在王家后，慢慢爱上了王家宽。&nbsp;朱灵得知自己不慎怀上身孕后把自己关在屋子里，不明真相的众人为把她喊出来唱山歌时，发现蔡玉珍原来是哑巴。朱灵出来后，王家宽因无法承受打击离家出走。不久，王家宽恢复平静回到村子，有心的朱灵开始撮合他与蔡玉珍。</span></p>', '/uploads/image/20190630/49d3b1e1afd66033673345404a1edd04.jpg', 'https://v3.mjshcn.com:987/20190629/wtTfZEB5/index.m3u8', '0', '0', '1561891193', '3');
INSERT INTO `b_movie` VALUES ('44', '撕心裂肺', '蒂亚·卡雷尔,戴尔·米德基夫,理查德·布基', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">影片《撕心裂肺》是由AlexanderStuart执导的惊悚片。影片的女主人公Vicki（蒂亚·卡雷尔饰）是一位医学教授。某一天，她收到一个匿名邮件，她必须要在48小时拯救她的丈夫和女儿。绑匪把她的丈夫和女儿放在了一个废弃的医疗中心，Vicki必须一个人去寻找谁是绑匪，谜底也慢慢揭开，面对危急的形式，丈夫和女儿只能选择一人，他会如何选择？是谁绑架了他们？此人如此做的目的又是什么？谜底终将揭晓，然而结果会否出人意料？</span></p>', '/uploads/image/20190630/6922492d5e1358d326f2f4373aef8438.jpg', 'https://v3.mjshcn.com:987/20190629/gtgyWwjy/index.m3u8', '0', '0', '1561891265', '3');
INSERT INTO `b_movie` VALUES ('45', '崎路父子情', '科林·费尔斯,吉姆·布劳德本特,茱丽叶特·斯蒂文森', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">本片根据布雷克·莫里森同名小说改编，由曾拍摄《她比烟花寂寞》和《导购女郎》的著名导演安南德·图克尔执导。四十岁的专栏作家布雷克·莫里森（柯林·费斯饰）刚辞了工作，准备创作一部小说。父亲阿瑟（吉姆·布劳特本饰）身患绝症的消息，让他一些字陷入了对过去的回忆：父亲专制的爱不仅带走了童年短暂的快乐，还使父子二人的感情渐渐疏远。当成年的布雷克不断和父亲对抗时，父亲也在不断与病魔斗争。可是，即便到了此刻，对于记忆中女人贝蒂（萨拉·兰凯萨饰）和父亲的关系，布雷克始终无法释怀，面对即将离世的父亲，已为人父的布雷克终于明白，缠绕心中多年的疑问，是时候找到答案了。</span></p>', '/uploads/image/20190630/5b4c5c11d0c2a5da3779729f7e83b292.jpg', 'https://v3.mjshcn.com:987/20190629/4ko10FGP/index.m3u8', '0', '1', '1561891756', '3');
INSERT INTO `b_movie` VALUES ('46', '拳速反击', 'PeerawatHerabat,NattanunJantarawetch', '2.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">真实故事改编，发生在风光明媚的泰国PP岛上，在这来自世界各地的拳手齐聚一堂，要来挑战泰拳拳王宝座。故事的主人翁肯，从小在孤儿院长大，他的父亲在拳击比赛中丧生，肯长大后立誓要替父复仇。他的泰拳师傅担心他重蹈覆辙，带他远离是非之地专心习武，多年后，肯重返PP岛，岛上仍被恶势力K-1所主宰，亦是他的杀父仇家，肯是否有办法记取师傅教训,泰拳真谛不是要击倒对手，而是要让对手输的心服口服。</span></p>', '/uploads/image/20190630/e116fede5a1a82f6bfdd9fc09e1cafb3.jpg', 'https://v3.mjshcn.com:987/20190629/n7aF0vu9/index.m3u8', '0', '1', '1561891735', '2');
INSERT INTO `b_movie` VALUES ('47', '美国杀人狂', 'CorinNemec,JenNikolaisen,DavidDeLuise', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">影片根据美国真实案件改编而成，在小镇上，经常出现年轻女子被莫名其妙的杀死，后来警察调查才知道是一个病态杀人魔所为，但是他们没有证据，只能慢慢调查。究竟是什么让杀手变成了变态杀手？</span></p>', '/uploads/image/20190630/e0286bfdbb4d85365c246a908c341d45.jpg', 'https://v3.mjshcn.com:987/20190629/AdfiVUCt/index.m3u8', '0', '1', '1561891687', '2');
INSERT INTO `b_movie` VALUES ('48', '黑衣人3', '威尔·史密斯,汤米·李·琼斯,乔什·布洛林,杰梅奈·克莱门特', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">月球最高监狱，穷凶极恶的伯格罗多星人鲍里斯（杰梅奈·克莱门特 Jemaine Clement 饰）越狱逃亡。他在1969年犯下邪恶罪行，最终被年轻的K（乔什·布洛林 Josh Brolin 饰） 砍掉一只手臂，送入监狱。此次他逃脱的目的，就是穿越时空杀死K。与此同时，J （威尔·史密斯 Will Smith 饰）和K（汤米·李·琼斯 Tommy Lee Jones 饰）一如既往处理各种外星人引起的混乱，因对鲍里斯的案件三缄其口，两个好搭档心生隔阂。崭新的一天，J发现K消失不见，周围更几乎没人知道K这个人。此时的K已在40年前被鲍里斯杀死，他所建立的A网也不复存在。在这危急时刻，伯格罗多星人大举入侵地球。为了保护家园，J必须穿越时空再次改变历史……©豆瓣</span></p>', '/uploads/image/20190630/df043a71efb44cb098ba4ba8a0863654.jpg', 'https://www.hbkdtz.com:987/20190622/nX81MC7t/index.m3u8', '0', '1', '1561891532', '2');
INSERT INTO `b_movie` VALUES ('49', '火线反攻', '乔什·杜哈明,布鲁斯·威利斯,罗莎里奥·道森,文森特·多诺费奥', '5.0', '0', '<p><span style=\"color: rgb(88, 88, 88); font-family: Tahoma, \" hiragino=\"\" sans=\"\" font-size:=\"\" background-color:=\"\">勇猛果敢的杰里米·科曼（乔什·杜哈明JoshDuhamel饰）是一名备受欢迎的消防员，某晚在执行完任务后，他进入便利店购物，却遭遇率领手下抢夺地盘的黑帮头子戴维·黑根（文森特·诺费奥VincentD&#39;Onofrio饰），店长及其家人被杀，杰里米侥幸逃脱。之后他作为证人指认凶手，但对戴维为人极其了解的警官迈克·塞拉（布鲁斯·威利斯BruceWillis饰）劝说杰里米接受证人保护，并隐姓埋名，以防止遭到戴维的无情报复。但是他的行踪很快被戴维一伙发现，他与女友塔莉亚（罗莎里奥·道森RosarioDawson饰）遭到伏击，女友更身受重伤。穷凶极恶的戴维，势要将杰里米逼向死路，困兽犹斗，杰里米展开绝死反抗……</span></p>', '/uploads/image/20190630/0445b26e3511ecfb18117816ef0d1528.jpg', 'https://www.hbkdtz.com:987/20190622/GioPDzsu/index.m3u8', '0', '1', '1561891614', '2');

-- ----------------------------
-- Table structure for b_movie_cate
-- ----------------------------
DROP TABLE IF EXISTS `b_movie_cate`;
CREATE TABLE `b_movie_cate` (
  `movie_cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name_en` varchar(20) NOT NULL DEFAULT '',
  `type_name_ch` varchar(20) NOT NULL DEFAULT '',
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '0正常，1删除',
  `index` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`movie_cate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_movie_cate
-- ----------------------------
INSERT INTO `b_movie_cate` VALUES ('1', 'kehuan', '科幻电影', '0', 'A1');
INSERT INTO `b_movie_cate` VALUES ('2', 'dongzuo', '动作电影', '0', 'A2');
INSERT INTO `b_movie_cate` VALUES ('3', 'juqing', '剧情电影', '0', 'A3');
INSERT INTO `b_movie_cate` VALUES ('4', 'zainandianying', '灾难电影应', '1', 'A4');

-- ----------------------------
-- Table structure for b_nav
-- ----------------------------
DROP TABLE IF EXISTS `b_nav`;
CREATE TABLE `b_nav` (
  `nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_type` varchar(20) NOT NULL DEFAULT '',
  `nav_name` varchar(50) NOT NULL DEFAULT '',
  `nav_value` text,
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_del` int(11) DEFAULT '0' COMMENT '0正常 1删除',
  `is_show` int(11) NOT NULL DEFAULT '0' COMMENT '0显示 1隐藏',
  PRIMARY KEY (`nav_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_nav
-- ----------------------------
INSERT INTO `b_nav` VALUES ('1', 'cate', '亚洲新闻', '4', '0', '0', '0');
INSERT INTO `b_nav` VALUES ('2', 'cate', '非洲新闻', '5', '0', '0', '0');
INSERT INTO `b_nav` VALUES ('3', 'cate', '美洲新闻', '6', '0', '0', '0');
INSERT INTO `b_nav` VALUES ('4', 'art_link', '关于我们', '12', '0', '0', '0');
INSERT INTO `b_nav` VALUES ('5', 'in_link', '内部链接', '/html/123.html', '0', '0', '0');
INSERT INTO `b_nav` VALUES ('6', 'out_link', '外部链接', 'http://www.baidu.com', '0', '0', '0');

-- ----------------------------
-- Table structure for b_node
-- ----------------------------
DROP TABLE IF EXISTS `b_node`;
CREATE TABLE `b_node` (
  `node_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `sort` varchar(50) NOT NULL DEFAULT '50',
  `isshow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0显示 1不显示',
  PRIMARY KEY (`node_id`),
  KEY `pid` (`pid`),
  KEY `status` (`isshow`),
  KEY `name` (`name`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of b_node
-- ----------------------------
INSERT INTO `b_node` VALUES ('1', '0', '', '常用操作', '', '0', '0');
INSERT INTO `b_node` VALUES ('2', '1', 'article/index', '文章管理', '', '1-1', '0');
INSERT INTO `b_node` VALUES ('4', '1', 'cate/index', '分类管理', '', '2-1', '0');
INSERT INTO `b_node` VALUES ('5', '1', 'label/index', '标签管理', '', '3-1', '0');
INSERT INTO `b_node` VALUES ('6', '1', 'comment/index', '评论管理', '', '4-1', '0');
INSERT INTO `b_node` VALUES ('8', '0', '', '系统管理', '', '0', '0');
INSERT INTO `b_node` VALUES ('9', '8', 'manage/index', '管理员列表', '', '1-1', '0');
INSERT INTO `b_node` VALUES ('10', '8', 'setup/index', '网站设置', '', '3-1', '0');
INSERT INTO `b_node` VALUES ('11', '8', 'userinfo/index', '个人信息', '', '4-1', '0');
INSERT INTO `b_node` VALUES ('20', '8', 'manage/del', '管理员删除', '', '1-2', '1');
INSERT INTO `b_node` VALUES ('21', '8', 'manage/add', '管理员新增', '', '1-3', '1');
INSERT INTO `b_node` VALUES ('22', '8', 'manage/upd', '管理员编辑', '', '1-4', '1');
INSERT INTO `b_node` VALUES ('23', '8', 'group/index', '权限分组', '', '2-1', '0');
INSERT INTO `b_node` VALUES ('24', '8', 'group/add', '新增分组', '', '2-2', '1');
INSERT INTO `b_node` VALUES ('25', '8', 'group/del', '分组删除', '', '2-3', '1');
INSERT INTO `b_node` VALUES ('26', '8', 'group/upd', '分组编辑', '', '2-4', '1');
INSERT INTO `b_node` VALUES ('27', '1', 'label/add', '标签新增', '', '3-2', '1');
INSERT INTO `b_node` VALUES ('28', '1', 'label/upd', '标签标签', '', '3-3', '1');
INSERT INTO `b_node` VALUES ('29', '1', 'label/del', '标签删除', '', '3-4', '1');
INSERT INTO `b_node` VALUES ('30', '1', 'cate/add', '新增分类', '', '2-2', '1');
INSERT INTO `b_node` VALUES ('31', '1', 'cate/upd', '编辑分类', '', '2-3', '1');
INSERT INTO `b_node` VALUES ('32', '1', 'cate/del', '删除分类', '', '2-4', '1');
INSERT INTO `b_node` VALUES ('33', '8', 'userinfo/pwd', '修改密码', '', '5-1', '1');
INSERT INTO `b_node` VALUES ('34', '1', 'comment/shield', '评论屏蔽', '', '4-2', '1');
INSERT INTO `b_node` VALUES ('35', '1', 'comment/del', '评论删除', '', '4-3', '1');
INSERT INTO `b_node` VALUES ('36', '1', 'article/content', '文章内容', '', '1-2', '1');
INSERT INTO `b_node` VALUES ('37', '1', 'article/add', '文章新增', '', '1-3', '1');
INSERT INTO `b_node` VALUES ('38', '1', 'article/del', '文章删除', '', '1-4', '1');
INSERT INTO `b_node` VALUES ('39', '1', 'article/upd', '文章编辑', '', '1-5', '1');
INSERT INTO `b_node` VALUES ('40', '1', 'member/index', '会员管理', '', '5-1', '0');
INSERT INTO `b_node` VALUES ('41', '1', 'member/del', '会员删除', '', '5-2', '1');
INSERT INTO `b_node` VALUES ('42', '1', 'feedback/index', '问题反馈', '', '6-1', '0');
INSERT INTO `b_node` VALUES ('43', '1', 'movie_cate/index', '电影分类', '', '7-1', '0');
INSERT INTO `b_node` VALUES ('44', '1', 'movie_cate/add', '电影分类新增', '', '7-2', '1');
INSERT INTO `b_node` VALUES ('45', '1', 'movie_cate/upd', '电影分类编辑', '', '7-3', '1');
INSERT INTO `b_node` VALUES ('46', '1', 'movie_cate/del', '电影分类删除', '', '7-4', '1');
INSERT INTO `b_node` VALUES ('47', '1', 'movie/index', '电影管理', '', '8-1', '0');
INSERT INTO `b_node` VALUES ('48', '1', 'movie/content', '电影简介', '', '8-2', '1');
INSERT INTO `b_node` VALUES ('49', '1', 'movie/play', '电影播放', '', '8-3', '1');
INSERT INTO `b_node` VALUES ('50', '1', 'movie/add', '电影添加', '', '8-4', '1');
INSERT INTO `b_node` VALUES ('51', '1', 'movie/upd', '电影编辑', '', '8-5', '1');
INSERT INTO `b_node` VALUES ('52', '1', 'movie/del', '电影删除', '', '8-6', '1');

-- ----------------------------
-- Table structure for b_setup
-- ----------------------------
DROP TABLE IF EXISTS `b_setup`;
CREATE TABLE `b_setup` (
  `setup_id` int(1) NOT NULL DEFAULT '1',
  `title` varchar(50) NOT NULL DEFAULT '',
  `copyright` varchar(255) NOT NULL DEFAULT '',
  `site_state` int(11) NOT NULL DEFAULT '0' COMMENT '0开启 1关闭',
  `comment` int(11) NOT NULL DEFAULT '0' COMMENT '0开启 1关闭',
  PRIMARY KEY (`setup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of b_setup
-- ----------------------------
INSERT INTO `b_setup` VALUES ('1', '电影吧管理系统', 'Copyright Your WebSite.Some Rights Reserved.', '0', '0');
