<?php
namespace app\api\controller;
use think\Controller;
use app\api\model\Lunbo as LunboModel;
use app\api\model\Movie as MovieModel;

class Index extends Common
{
    /**
     * 查询轮播图
     */
    public function selectLunbo()
    {
        // 实例化对象
        $lunbo = new LunboModel();
        // 取轮播图返回值
        $list = $lunbo->select();
        // 返回json格式数据
        if ( $list ) {
            return json(['data'=>['list'=>$list],'code'=>0,'message'=>'ok']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'no']);
        }
    }

    /**
     * 按类型查询电影
     */
    public function selectTypeMovie()
    {
        // 实例化对象
        $movie = new MovieModel();
        // 取热门推荐返回值
        $list = $movie->selTypeMovie(input('param.type'));
        // 返回json格式数据
        if ( $list ) {
            return json(['data'=>['list'=>$list],'code'=>0,'message'=>'ok']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'no']);
        }
    }

    /**
     * 查询分类电影
     */
    public function selectCateMovie()
    {
        // 实例化对象
        $movie = new MovieModel();
        // 取分类电影返回值
        $list = $movie->selCateMovie(input('param.index'));
        $cate = $movie->selCateName(input('param.index'));
        // 返回json格式数据
        if ( $list ) {
            return json(['data'=>['type_name_ch'=>$cate['type_name_ch'],'list'=>$list],'code'=>0,'message'=>'ok']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'no']);
        }
    }
}
