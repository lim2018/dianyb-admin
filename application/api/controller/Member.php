<?php
namespace app\api\controller;

use app\api\model\Member as MemberModel;

class Member extends Base
{
    /**
     * 修改密码
     */
    public function updPwdByPwd()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $member = new MemberModel();
            // 接收数据
            $member_id = input('post.member_id');
            $oldPassword = input('post.oldPassword');
            $newPassword = input('post.newPassword');
            // 取热门推荐返回值
            $result = $member->updPwdByPwd($oldPassword,$newPassword,$member_id);
            // 返回json格式数据
            return $result;
        }
    }
}
