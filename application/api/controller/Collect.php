<?php
namespace app\api\controller;

use app\api\model\Collect as CollectModel;

class Collect extends Base
{
    /**
     * 收藏收藏
     */
    public function selCollectById()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $collect = new CollectModel();
            // 取热门推荐返回值
            $list = $collect->selCollectById(input('param.member_id'));
            // 返回json格式数据
            if ($list) {
                return json(['data' => ['list' => $list], 'code' => 0, 'message' => 'ok']);
            } else {
                return json(['data' => '', 'code' => 1, 'message' => 'no']);
            }
        }
    }

    /**
     * 添加收藏
     */
    public function addCollect()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $collect = new CollectModel();
            // 接收数据
            $data = input('post.');
            // 取热门推荐返回值
            $result = $collect->addCollect($data);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'ok']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'no']);
            }
        }
    }

    /**
     * 删除收藏
     */
    public function delCollect()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $collect = new CollectModel();
            // 接收数据
            $collect_id = input('post.collect_id');
            // 取热门推荐返回值
            $result = $collect->delCollect($collect_id);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'删除成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'删除失败']);
            }
        }
    }

    /**
     * 收藏状态
     */
    public function isCollect()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $collect = new CollectModel();
            // 接收数据
            $member_id = input('post.member_id');
            $movie_id = input('post.movie_id');
            // 取热门推荐返回值
            $result = $collect->isCollect($member_id,$movie_id);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'ok']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'no']);
            }
        }
    }
}
