<?php
namespace app\api\controller;
use think\Controller;
use app\api\model\Movie as MovieModel;

class Details extends Common
{
    /**
     * 查询电影详情
     */
    public function selMovieDetails()
    {
        // 实例化对象
        $movie = new MovieModel();
        // 取轮播图返回值
        $data = $movie->selMovieDetails(input('param.movie_id'));
        // 返回json格式数据
        if ( $data ) {
            return json(['data'=>$data,'code'=>0,'message'=>'ok']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'no']);
        }
    }
}
