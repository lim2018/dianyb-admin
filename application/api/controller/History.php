<?php
namespace app\api\controller;

use app\api\model\History as HistoryModel;

class History extends Base
{
    /**
     * 历史记录
     */
    public function selHistoryById()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $history = new HistoryModel();
            // 取热门推荐返回值
            $list = $history->selHistoryById(input('param.member_id'));
            // 返回json格式数据
            if ($list) {
                return json(['data' => ['list' => $list], 'code' => 0, 'message' => 'ok']);
            } else {
                return json(['data' => '', 'code' => 1, 'message' => 'no']);
            }
        }
    }

    /**
     * 添加记录
     */
    public function addHistory()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $history = new HistoryModel();
            // 接收数据
            $data = input('post.');
            // 取热门推荐返回值
            $result = $history->addHistory($data);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'ok']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'no']);
            }
        }
    }

    /**
     * 删除记录
     */
    public function delHistory()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $history = new HistoryModel();
            // 接收数据
            $history_id = input('post.history_id');
            // 取热门推荐返回值
            $result = $history->delHistory($history_id);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'删除成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'删除失败']);
            }
        }
    }
}
