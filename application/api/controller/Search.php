<?php
namespace app\api\controller;
use think\Controller;
use app\api\model\Movie as MovieModel;

class Search extends Common
{
    /**
     * 查询搜索列表
     */
    public function selMovieDetails()
    {
        $movie= new MovieModel();
        $movie_title = input('param.movie_title');
        $page = input('param.page');
        $listRows = input('param.listRows');
        $data = $movie->search($movie_title,$page,$listRows);
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }
}
