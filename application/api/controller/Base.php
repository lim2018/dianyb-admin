<?php
namespace app\api\controller;
use think\Controller;
use think\Request;
use app\api\service\Token;
class Base extends Common
{
    /**
     * 构造函数
     */
    public function _initialize()
    {
        $token = Request::instance()->header('token');
        $data = Token::verifyToken($token);
        if (!$data) {
            $arr = ['data'=>'','code'=>2,'message'=>'token错误，无访问权限'];
            echo json_encode($arr,JSON_UNESCAPED_UNICODE);die;
        }
    }
}
