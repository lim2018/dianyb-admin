<?php
namespace app\api\controller;

use app\api\model\Feedback as FeedbackModel;

class Feedback extends Base
{
    /**
     * 添加记录
     */
    public function addFeedback()
    {
        //判断提交方式
        if (request()->isPost()) {
            // 实例化对象
            $feedback= new FeedbackModel();
            // 接收数据
            $data = input('post.');
            // 取热门推荐返回值
            $result = $feedback->addFeedback($data);
            // 返回json格式数据
            if ( $result ) {
                return json(['data'=>'','code'=>0,'message'=>'反馈成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'反馈失败']);
            }
        }
    }
}
