<?php
namespace app\api\controller;
use think\Controller;
use think\Request;
use app\api\service\Token;
class Common extends Controller
{
    /**
     * 构造函数
     */
    public function _initialize()
    {
        // 允许所有域名访问
        header('Access-Control-Allow-Origin:*');
    }
}
