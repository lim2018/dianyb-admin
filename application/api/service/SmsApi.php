<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/6/17
 * Time: 0:48
 */

namespace app\api\service;
use think\Cache;

class SmsApi
{
    /**
     * 请求聚合短信接口
     */
    public function sendMsg($phone_num)
    {
        $url = "http://v.juhe.cn/sms/send";
        $code = $this->createMsgCode($phone_num);
        $params = array(
            'key'   => '3f4e907e6495c29a66fa552efc2acfc6', //您申请的APPKEY
            'mobile'    => $phone_num, //接受短信的用户手机号码
            'tpl_id'    => '167197', //您申请的短信模板ID，根据实际情况修改
            'tpl_value' =>'#code#='.$code.'&#company#=电影吧' //您设置的模板变量，根据实际情况修改
        );

        $paramstring = http_build_query($params);
        $content = req_curl($url, $paramstring);
        $result = json_decode($content, true);
        $result['msg_code'] = Cache::get('phone:'.$phone_num);
        if ($result['error_code'] == 0) {
            return json(['data'=>$result,'code'=>0,'message'=>'发送成功']);
        } else {
            return json(['data'=>$result,'code'=>1,'message'=>'发送失败']);
        }
    }

    /**
     * 生成验证码
     */
    protected function createMsgCode($phone_num)
    {
        $seed = time();                   // 使用时间作为种子源
        mt_srand($seed);                  // 播下随机数发生器种子
        $code = rand(100000, 999999);      // 根据种子生成 1000000~9999999 之间的随机数，如果 $seed 值固定，则生成的随机数也不变
        Cache::set('phone:'.$phone_num,$code,600);
        return $code;
    }
}