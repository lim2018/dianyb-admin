<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/6/17
 * Time: 0:48
 */

namespace app\api\service;

use think\Db;

class Token
{
    /**
     * 获取token值
     */
    public static function getToken()
    {
        $randStr = getRandomStr(32);
        $timeStr = time();
        $code = "zfidDOffmf";
        return md5($randStr.$timeStr.$code);
    }

    /**
     * 验证token值
     */
    public static function verifyToken($token)
    {
        $data = Db::name("member")->where(['token'=>$token,'is_del'=>0])->find();
        return $data;
    }
}