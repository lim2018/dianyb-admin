<?php
namespace app\api\model;
use \think\Db;
use \think\Model;
class Movie extends Model
{
    /**
     * 查询搜索列表
     */
    public function search($movie_title,$page,$listRows)
    {
        //查询条件
        if ($movie_title !== '') {
            $where['movie_title'] = $movie_title;
        }
        $where['is_del'] = 0;
        //总记录数
        $count = Db::name('movie')->where($where)->count();
        //总页数
        $totalPage = ceil($count/ $listRows);
        //数据
        $list = Db::name("movie")->where($where)->order('movie_id desc')->page($page,$listRows)->select();
        return [
            'list' => $list,
            'count' => $count,
            'totalPage' => $totalPage,
            'page' => $page,
            'listRows' => $listRows
        ];
    }
    /**
     * 按类型查询
     */
	public function selTypeMovie($type)
    {
        $list = Db::name("movie")->where(['type'=>$type,'is_del'=>0])->order('movie_id desc')->limit(6)->select();
        return $list;
    }

    /**
     * 按分类查询
     */
    public function selCateMovie($index)
    {
        $field = [
            'm.movie_id' => 'movie_id',
            'm.movie_title' => 'movie_title',
            'm.actor' => 'actor',
            'm.score' => 'score',
            'm.browse_num' => 'browse_num',
            'm.face_pic' => 'face_pic',
            'm.movie_path' => 'movie_path',
            'c.type_name_ch' => 'type_name_ch',
            'c.index' => 'index'
        ];
        $list = Db::name("movie")
            ->alias("m")
            ->field($field)
            ->join("__MOVIE_CATE__ c","m.movie_cate_id = c.movie_cate_id","left")
            ->where(['c.index'=>$index,'m.is_del'=>0])->order('m.movie_id desc')->limit(6)->select();
        return $list;
    }

    /**
     * 查询分类名称
     */
    public function selCateName($index)
    {
        $cate = Db::name("movie_cate")->field('type_name_ch')->where(['index'=>$index,'is_del'=>0])->find();
        return $cate;
    }

    /**
     * 查询电影详情
     */
    public function selMovieDetails($movie_id)
    {
        $movie = Db::name("movie")->where(['movie_id'=>$movie_id,'is_del'=>0])->find();
        return $movie;
    }

}
