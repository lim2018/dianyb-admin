<?php
namespace app\api\model;
use \think\Db;
use \think\Model;
class Feedback extends Model
{
    /**
     * 添加记录
     */
    public function addFeedback($data)
    {
        $data['add_time'] = time();
        $this->data($data);
        $result = $this->save();
        return $result;
    }
}
