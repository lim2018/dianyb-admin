<?php
namespace app\api\model;
use \think\Db;
use \think\Model;
use \app\api\service\Token;
use think\Cache;
class Member extends Model
{
    /**
     * 账号密码登录
     */
    public function loginByPwd($phone_num,$password)
    {
        // 数据库比对
        $member = Db::name('member')->where(['phone_num' => $phone_num])->find();
        if (empty($member)) {
            //说明手机号错误
            return json(['data'=>'','code'=>1,'message'=>'手机号错误']);
        }
        if($member['password'] !=  md5($password)) {
            //说明密码错误
            return json(['data'=>'','code'=>1,'message'=>'密码错误']);
        }
        // 生成token存入数据库
        $token = Token::getToken();
        $data['token'] = $token;
        $result = $this->upd($member['member_id'],$data);
        if ($result >= 0) {
            $member['token'] = $token;
            unset($member['password']);
            return json(['data'=>$member,'code'=>0,'message'=>'登录成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'登录失败，未知原因']);
        }
    }

    /**
     * 短信验证码登录
     */
    public function loginByMsg($phone_num,$msg_code)
    {
        // 数据库比对
        $member = Db::name('member')->where(['phone_num' => $phone_num])->find();
        if (empty($member)) {
            //说明手机号错误
            return json(['data'=>'','code'=>1,'message'=>'手机号错误']);
        }
        if ($msg_code != Cache::get('phone:'.$phone_num)) {
            //说明短信验证码错误
            return json(['data'=>'','code'=>1,'message'=>'验证码错误']);
        }
        // 生成token存入数据库
        $token = Token::getToken();
        $data['token'] = $token;
        $result = $this->upd($member['member_id'],$data);
        if ($result >= 0) {
            $member['token'] = $token;
            unset($member['password']);
            return json(['data'=>$member,'code'=>0,'message'=>'登录成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'登录失败，未知原因']);
        }
    }

    /**
     * 修改
     */
    public function upd($member_id,$data)
    {
        $result = $this->save($data,['member_id' => $member_id]);
        return $result;
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $data['add_time'] = time();
        $data['password'] = md5($data['password']);
        unset($data['msg_code']);
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 修改密码
     */
    public function updPwdByPwd($oldPassword,$newPassword,$member_id)
    {
        // 对比就密码是否正确
        $oldPassword = md5($oldPassword);
        $member = Db::name('member')->where(['member_id' => $member_id,'password' => $oldPassword])->find();
        if (!$member) {
            return json(['data'=>'','code'=>1,'message'=>'旧密码错误']);
        }
        //修改新密码
        $data['password'] = md5($newPassword);
        $result = $this->save($data,['member_id' => $member_id]);
        if ($result >= 0){
            return json(['data'=>'','code'=>0,'message'=>'修改成功']);
        } else {
            return json(['data'=>'','code'=>0,'message'=>'修改失败']);
        }
    }
}
