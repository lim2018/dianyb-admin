<?php
namespace app\api\model;
use \think\Db;
use \think\Model;
class Lunbo extends Model
{
    /**
     * 查询
     */
	public function select()
    {
        $list = Db::name("lunbo")->where(['is_del'=>0])->select();
        return $list;
    }

}
