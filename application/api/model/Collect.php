<?php
namespace app\api\model;
use \think\Db;
use \think\Model;
class Collect extends Model
{
    /**
     * 收藏收藏
     */
    public function selCollectById($member_id)
    {
        $field = [
            'h.collect_id' => 'collect_id',
            'h.add_time' => 'add_time',
            'm.movie_id' => 'movie_id',
            'm.movie_title' => 'movie_title',
            'm.face_pic' => 'face_pic'
        ];
        $list = Db::name("collect")
            ->alias("h")
            ->field($field)
            ->join("__MOVIE__ m","h.movie_id = m.movie_id","left")
            ->where(['h.is_del'=>0,'h.member_id'=>$member_id])->order('h.add_time desc')->limit(100)->select();
        foreach ($list as $key => $value) {
            $list[$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
        }
        return $list;
    }

    /**
     * 添加收藏
     */
    public function addCollect($data)
    {
        $data['add_time'] = time();
        unset($data['msg_code']);
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 删除收藏
     */
    public function delCollect($collect_id)
    {
        if ($collect_id > 0) {
            $result = $this->save(['is_del' => 1],['collect_id' => $collect_id]);
            return $result;
        }
    }

    /**
     * 收藏状态
     */
    public function isCollect($member_id,$movie_id)
    {
        $result = Db::name("collect")->where(['member_id'=>$member_id,'movie_id'=>$movie_id])->find();
        return $result;
    }
}
