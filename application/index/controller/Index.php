<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\Index as IndexModel;
class Index extends Base
{
    /**
     * 网站首页
     */
    public function index()
    {
//        $IndexModel = new IndexModel();
//        $list = $IndexModel->search();
//        $top = $IndexModel->selTop();
//        $page = $list->render();
//        $this->assign([
//            'list'  => $list,
//            'page' => $page,
//            'top' => $top
//        ]);
//        return $this->fetch();
        $this->redirect('admin/index/index');
    }

    /**
     * 列表搜索
     */
    public function search()
    {
        return $this->fetch();
    }

    /**
     * 最新文章
     */
    public function selArtNew()
    {
        $IndexModel = new IndexModel();
        $list = $IndexModel->selArtNew();
        $data['list'] = $list;
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }

    /**
     * 热门文章
     */
    public function selArtHot()
    {
        $IndexModel = new IndexModel();
        $list = $IndexModel->selArtHot();
        $data['list'] = $list;
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }

    /**
     * 最新评论
     */
    public function selCommentNew()
    {
        $IndexModel = new IndexModel();
        $list = $IndexModel->selCommentNew();
        $data['list'] = $list;
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }

    /**
     * 单页内容
     */
    public function page()
    {
        $IndexModel = new IndexModel();
        $article_id = input('param.article_id');
        $article = $IndexModel->findArtById($article_id);
        $commentNum = $IndexModel->selCommentNum($article_id);
        $prev = $IndexModel->prevArt($article_id);
        $next = $IndexModel->nextArt($article_id);
        $IndexModel->addBrowseNum($article_id,$article);
        $this->assign([
            'article' => $article,
            'commentNum' => $commentNum,
            'prev' => $prev,
            'next' => $next
        ]);
        return $this->fetch();
    }

    /**
     * 查询评论
     */
    public function selCommentByArtId()
    {
        $IndexModel = new IndexModel();
        $article_id = input('param.article_id');
        $page = input('param.page');
        $listRows = input('param.listRows');
        $data = $IndexModel->selCommentByArtId($article_id,$page,$listRows);
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }

    /**
     * 新增评论
     */
    public function addComment()
    {
        $IndexModel = new IndexModel();
        $data = input('post.');
        $result = $IndexModel->addComment($data);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'新增成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'新增失败']);
        }
    }

    /**
     * 常用标签
     */
    public function selLabel()
    {
        $IndexModel = new IndexModel();
        $list = $IndexModel->selLabel();
        $data['list'] = $list;
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }

    /**
     * 统计数量
     */
    public function statistics()
    {
        $IndexModel = new IndexModel();
        $data = $IndexModel->statistics();
        if ($data) {
            return json(['data'=>$data,'code'=>0,'message'=>'查询成功']);
        } else {
            return json(['data'=>$data,'code'=>1,'message'=>'无结果']);
        }
    }
}
