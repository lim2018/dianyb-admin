<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Index as IndexModel;

class Base extends Controller
{
    /**
     * 构造函数
     */
    public function _initialize()
    {
        $IndexModel = new IndexModel();
        $list = $IndexModel->getNavLink();
        $setup = $IndexModel->getSetUP();
        $this->assign([
            'nav_list'  => $list,
            'setup' => $setup
        ]);
    }
}
