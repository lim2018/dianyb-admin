<?php
namespace app\index\model;
use \think\session;
use \think\Model;
use \think\Db;
class Index extends Model
{
    /**
     * 文章搜索
     */
    public function search()
    {
        $list = Db::name("article")->alias("a")
            ->field("a.article_id, a.art_title,a.keyword,a.describe,a.label,a.cate_id,
            a.state,a.is_del,a.last_time,a.release_time,a.author_id,a.art_content,
            a.forbid_comment,a.top,a.browse_num,c.cate_name,m.username as author_user")
            ->join("__CATE__ c","a.cate_id = c.cate_id","left")
            ->join("__MANAGE__ m","a.author_id = m.manage_id","left")
            ->order('a.release_time desc')
            ->where(['a.state'=>1,'a.is_del'=>0 ,'a.top' => 1])->paginate(10)->each(function($item, $key){
                $item['release_time'] = date('Y-m-d',$item['release_time']);
                $item['com_num'] = $this->selCommentNum($item['article_id']);
                $item['art_img'] = $this->getArtImg($item['art_content']);
                $item['art_content'] = mb_substr($this->cutstr_html($item['art_content']),3,100,'utf-8');
                return $item;
            });
        return $list;
    }

    /**
     * 查询置顶
     */
    public function selTop()
    {
        $list = Db::name("article")->alias("a")
            ->field("a.article_id, a.art_title,a.keyword,a.describe,a.label,a.cate_id,
            a.state,a.is_del,a.last_time,a.release_time,a.author_id,a.art_content,
            a.forbid_comment,a.top,a.browse_num,c.cate_name,m.username as author_user")
            ->join("__CATE__ c","a.cate_id = c.cate_id","left")
            ->join("__MANAGE__ m","a.author_id = m.manage_id","left")
            ->order('a.release_time desc')
            ->where(['a.state'=>1,'a.is_del'=>0,'a.top' => 0])->paginate(2,1000)->each(function($item, $key){
                $item['release_time'] = date('Y-m-d',$item['release_time']);
                $item['com_num'] = $this->selCommentNum($item['article_id']);
                $item['art_img'] = $this->getArtImg($item['art_content']);
                $item['art_content'] = mb_substr($this->cutstr_html($item['art_content']),3,100,'utf-8');
                return $item;
            });
        return $list;
    }

    /**
     * 去除HTML标签
     * @param $string
     * @return string
     */
    public function cutstr_html($string){

        $string = strip_tags($string);

        $string = trim($string);

        $string = str_replace("\t","",$string);

        $string = str_replace("\r\n","",$string);

        $string = str_replace("\r","",$string);

        $string = str_replace("\n","",$string);

        $string = str_replace(" ","",$string);

        return trim($string);
    }

    /**
     * 获取图片
     */
    public function getArtImg($content)
    {
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
        $content = $content; //文章内容
        preg_match_all($pattern,$content,$matchContent);
        if(isset($matchContent[1][0])){
            $temp=$matchContent[1][0];
        }else{
            $temp="/uploads/no-image.jpg";//在相应位置放置一张命名为no-image的jpg图片
        }
        return $temp;
    }

    /**
     * 查询文章
     */
	public function findArtById($article_id)
    {
        $article = Db::name("article")->alias("a")
            ->field("a.article_id, a.art_title,a.keyword,a.describe,a.art_content,
            a.label,a.cate_id,a.state,a.is_del,a.last_time,a.release_time,a.author_id,
            a.forbid_comment,a.top,a.browse_num,c.cate_name,m.username as author_user")
            ->join("__CATE__ c","a.cate_id = c.cate_id","left")
            ->join("__MANAGE__ m","a.author_id = m.manage_id","left")
            ->where(['a.article_id'=>$article_id,'a.state'=>1,'a.is_del'=>0])->find();
        return $this->doLabel($article);
    }

    /**
     * 最新文章
     */
    public function selArtNew()
    {
        $list = Db::name("article")->field("article_id, art_title")->where(['state'=>1,'is_del'=>0])
            ->order('release_time desc')->limit(6)->select();
        return $list;
    }

    /**
     * 热门文章
     */
    public function selArtHot()
    {
        $list = Db::name("article")->field("article_id, art_title")->where(['state'=>1,'is_del'=>0])
            ->order('browse_num desc')->limit(6)->select();
        return $list;
    }

    /**
     * 最新评论
     */
    public function selCommentNew()
    {
        $list = Db::name("comment")->alias("c")
            ->field("c.comment_id,c.comment_user,c.comment,c.comment_time,c.article_id, a.art_title")
            ->join("__ARTICLE__ a","a.article_id = c.article_id","left")
            ->where(['c.is_show'=>0,'c.del'=>0])
            ->order('c.comment_time desc')->limit(6)->select();
        foreach ($list as $key => $value) {
            $list[$key]['comment_time'] = date('m月d日',$value['comment_time']);
        }
        return $list;
    }

    /**
     * 查询评论数
     */
    public function selCommentNum($article_id)
    {
        $comment = Db::name("comment")->where(['article_id'=>$article_id,'is_show'=>0,'del'=>0])->select();
        return count($comment);
    }

    /**
     * 查询评论
     */
    public function selCommentByArtId($article_id,$page,$listRows)
    {
        //查询条件
        $where = [
            'article_id'=>$article_id,
            'is_show'=>0,
            'del'=>0
        ];
        //总记录数
        $count = Db::name('comment')->where($where)->count();
        //总页数
        $totalPage = ceil($count/ $listRows);
        //数据
        $list = Db::name("comment")->where($where)->order('comment_id desc')->page($page,$listRows)->select();
        foreach ($list as $key => $value) {
            $list[$key]['comment_time'] = date('Y-m-d H:i',$value['comment_time']);
        }
        return [
            'list' => $list,
            'count' => $count,
            'totalPage' => $totalPage,
            'page' => $page,
            'listRows' => $listRows
        ];
    }

    /**
     * 评论新增
     */
    public function addComment($data)
    {
        $data['comment_time'] = time();
        $result = Db::name('comment')->insert($data);
        return $result;
    }

    /**
     * 处理标签
     */
    public function doLabel($article)
    {
        foreach ($article as $key => $value) {
            $article['labelArr'] = explode(',',$article['label']);
        }
        return $article;
    }

    /**
     * 上一篇文章
     */
    public function prevArt($article_id)
    {
        $where['article_id'] = ['<',$article_id];
        $where['is_del'] = 0;
        $where['state'] = 1;
        $prev = Db::name("article")->field('article_id,art_title')
            ->where($where)->order('article_id desc')->limit(0,1)->find();
        return $prev;
    }

    /**
     * 下一篇文章
     */
    public function nextArt($article_id)
    {
        $where['article_id'] = ['>',$article_id];
        $where['is_del'] = 0;
        $where['state'] = 1;
        $next = Db::name("article")->field('article_id,art_title')
            ->where($where)->limit(0,1)->find();
        return $next;
    }

    /**
     * 常用标签
     */
    public function selLabel()
    {
        $list = Db::name("label")->where(['is_del'=>0])->select();
        return $list;
    }

    /**
     * 增加浏览数
     */
    public function addBrowseNum($article_id,$article)
    {
        $article['browse_num'] = $article['browse_num'] + mt_rand(1000, 9999);
        $result = Db::name('article')->where(['article_id'=>$article_id])->update(['browse_num' => $article['browse_num']]);
        return $result;
    }

    /**
     * 统计数量
     */
    public function statistics()
    {
        $artNum = Db::name("article")->where(['state'=>1,'is_del'=>0])->count();
        $comNum = Db::name("comment")->where(['is_show'=>0,'del'=>0])->count();
        $broNum = Db::name("article")->where(['state'=>1,'is_del'=>0])->sum('browse_num');
        return [
            'artNum' => $artNum,
            'comNum' => $comNum,
            'broNum' => $broNum
        ];
    }

    /**
     * 取导航
     */
    public function getNavLink()
    {
        $list = Db::name("nav")->where(['is_show'=>0,'is_del'=>0])->order('sort asc')->select();
        return $list;
    }

    /**
     * 取网站设置
     */
    public function getSetUP(){
        $setup = Db::name("setup")->where(['setup_id'=>1])->find();
        return $setup;
    }
}
