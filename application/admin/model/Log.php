<?php
namespace app\admin\model;
use think\Session;
use \think\Model;
class Log extends Model
{
    /**
     * 查询分组信息
     */
    public function findsetup($setup_id)
    {
        $setup = $this->where(['setup_id'=>$setup_id])->find();
        return $setup->toArray();
    }

    /**
     * 修改
     */
    public function upd($setup_id,$data)
    {
        $result = $this->save($data,['setup_id' => $setup_id]);
        return $result;
    }

    /**
     * 按用户取日志
     */
    public function selByUser($type = '',$manage_id,$num = 10)
    {
        $list = $this->where(['type'=>$type,'manage_id'=>$manage_id])
            ->order("log_id desc")
            ->paginate($num)
            ->each(function($item, $key){
                $item->username = Session::get("admin")["username"];
                $item->log_value = json_decode($item->log_value,true);
            });
        return $list;
    }

    /**
     * 登录次数
     */
    public function logNum()
    {
        $list = $this->selByUser("login",Session::get("admin")["manage_id"],10000);
        $count = count($list);
        return $count;
    }

    /**
     * 写入日志
     */
    public function addLog($type = '',$log_value = '',$manage_id)
    {
        $data['type'] = $type;
        $data['log_value'] = $log_value;
        $data['manage_id'] = $manage_id;
        $this->data($data);
        $result = $this->save();
        return $result;
    }
}
