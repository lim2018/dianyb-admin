<?php
namespace app\admin\model;

use \think\Model;
class Label extends Model
{
    /**
     * 查询
     */
	public function sel()
    {
        $list = $this->where(['is_del' => 0])->order("Label_id asc")->select();
        return collection($list)->toArray();
    }

    /**
     * 删除
     */
    public function del($Label_id)
    {
        if ($Label_id > 0) {
            $result = $this->save(['is_del' => 1],['Label_id' => $Label_id]);
            return $result;
        }
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询分组信息
     */
    public function findLabel($label_id)
    {
        $Label = $this->where(['label_id'=>$label_id])->find();
        return $Label->toArray();
    }

    /**
     * 修改
     */
    public function upd($label_id,$data)
    {
        $result = $this->save($data,['label_id' => $label_id]);
        return $result;
    }
}
