<?php
namespace app\admin\model;

use \think\Model;
use \app\admin\service\Pinyin;
class MovieCate extends Model
{
    /**
     * 查询
     */
	public function sel()
    {
        $list = $this->where(['is_del' => 0])->order("movie_cate_id asc")->select();
        return collection($list)->toArray();
    }

    /**
     * 删除
     */
    public function del($movie_cate_id)
    {
        if ($movie_cate_id > 0) {
            $result = $this->save(['is_del' => 1],['movie_cate_id' => $movie_cate_id]);
            return $result;
        }
    }

    /**
     * 新增
     */
    public function add($data)
    {
        // 取最后一条数据
        $tempArr = $this->order('movie_cate_id desc')->limit(1)->select();
        if ($tempArr) {
            $tempArr = collection($tempArr)->toArray();
            $indexNum = explode('A',$tempArr[0]['index'])[1] + 1;
            $data['index'] = 'A'.$indexNum;
        } else {
            $data['index'] = 'A1';
        }
        // 中文名转化拼音
        $data['type_name_en'] = Pinyin::encode($data['type_name_ch'],'all');
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询分组信息
     */
    public function findMovieCate($movie_cate_id)
    {
        $movie_cate = $this->where(['movie_cate_id'=>$movie_cate_id])->find();
        return $movie_cate->toArray();
    }

    /**
     * 修改
     */
    public function upd($movie_cate_id,$data)
    {
        $result = $this->save($data,['movie_cate_id' => $movie_cate_id]);
        return $result;
    }
}
