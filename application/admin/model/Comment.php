<?php
namespace app\admin\model;

use \think\Model;
class Comment extends Model
{
    /**
     * 查询article
     */
	public function sel($type = 0,$keyword = "")
    {
        $where['c.del'] = 0;
        if ($type == 0) {
            $where['c.comment'] = ["like","%$keyword%"];
        }
        if ($type == 1) {
            $where['c.art_title'] = ["like","%$keyword%"];
        }
        $list = $this->alias("c")
            ->field('c.comment_id,c.comment_user,c.comment,c.reply,c.comment_time,c.is_show,c.del,
            a.art_title,m.username')
            ->join("__ARTICLE__ a","c.article_id = a.article_id","left")
            ->join("__MANAGE__ m","c.reply_id = m.manage_id","left")
            ->order("comment_time desc")
            ->where($where)
            ->paginate(15)
            ->each(function($item, $key){
                $item->comment_time = date("Y-m-d H:i",$item->comment_time);
                $item->reply_user = $item->username;
                //处理评论
                if (strlen($item->comment) > 10) {
                    $item->comment_new = mb_substr($item->comment,0,9,'utf-8').'...';
                } else {
                    $item->comment_new = $item->comment;
                }
                //处理回复
                if (strlen($item->reply) > 10) {
                    $item->reply_new = mb_substr($item->reply,0,9,'utf-8').'...';
                } else {
                    $item->reply_new = $item->reply;
                }
                //处理屏蔽
                if ($item->is_show == 0) {
                    $item->is_show = '<span style="color:green">正常显示</span>';
                } else {
                    $item->is_show = '<span style="color:red">已屏蔽</span>';
                }
            });
//        echo $this->getLastSql();
        return $list;
    }

    /**
     * 屏蔽
     */
    public function shield($comment_id)
    {
        if ($comment_id > 0) {
            $result = $this->save(['is_show' => 1],['comment_id' => $comment_id]);
            return $result;
        }
    }

    /**
     * 删除
     */
    public function del($comment_id)
    {
        if ($comment_id > 0) {
            $result = $this->save(['del' => 1],['comment_id' => $comment_id]);
            return $result;
        }
    }
}
