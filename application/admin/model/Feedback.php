<?php
namespace app\admin\model;

use \think\Model;
class Feedback extends Model
{
    /**
     * 查询
     */
	public function sel($phone_num = "")
    {
        $where = [];
        if (!empty($phone_num)) {
            $where['phone_num'] = ["like","%$phone_num%"];
        }
        $list = $this->alias("f")
            ->field([
                'f.feedback_id' => 'feedback_id',
                'f.content' => 'content',
                'f.add_time' => 'add_time',
                'm.member_id' => 'member_id',
                'm.nickname' => 'nickname',
                'm.phone_num' => 'phone_num'
            ])
            ->join("__MEMBER__ m","m.member_id = f.member_id","left")
            ->order("f.add_time desc")
            ->where($where)
            ->paginate(15)
            ->each(function($item, $key){
                $item->add_time = date("Y-m-d H:i",$item->add_time);
            });
        return $list;
    }
}
