<?php
namespace app\admin\model;

use \think\Model;
class Manage extends Model
{
    /**
     * 查询
     */
	public function sel($username = "")
    {
        $where = [];
        if (!empty($username)) {
            $where['username'] = ["like","%$username%"];
        }
        $list = $this->alias("m")
            ->join("__GROUP__ g","m.group_id = g.group_id","left")
            ->order("manage_id desc")
            ->where($where)
            ->paginate(15)
            ->each(function($item, $key){
                $item->login_time = date("Y-m-d H:i",$item->login_time);
            });
        return $list;
    }

    /**
     * 删除
     */
    public function del($manage_id)
    {
        if ($manage_id > 0) {
            $result = $this->where(["manage_id"=>$manage_id])->delete();
            return $result;
        }
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $data['add_time'] = time();
        $data['password'] = md5($data['password']);
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询用户信息
     */
    public function findUser($manage_id)
    {
        $manage = $this->where(['manage_id'=>$manage_id])->find();
        return $manage->toArray();
    }

    /**
     * 修改
     */
    public function upd($manage_id,$data)
    {
        if ($data['password'] == '') {
            $manage = $this->findUser($manage_id);
            $data['password'] == $manage['password'];
        }
        $result = $this->save($data,['manage_id' => $manage_id]);
        return $result;
    }
}
