<?php
namespace app\admin\model;
use \think\session;
use \think\Model;
class Movie extends Model
{
    /**
     * 查询
     */
	public function sel($movie_title = "")
    {

        $where['m.is_del'] = 0;
        if (!empty($movie_title)) {
            $where['m.movie_title'] = ["like","%$movie_title%"];
        }

        $field = [
            'm.movie_id' => 'movie_id',
            'm.movie_title' => 'movie_title',
            'm.actor' => 'actor',
            'm.score' => 'score',
            'm.browse_num' => 'score',
            'm.intro' => 'intro',
            'm.face_pic' => 'face_pic',
            'm.movie_path' => 'movie_path',
            'm.type' => 'movie_type',
            'm.add_time' => 'add_time',
            'c.movie_cate_id' => 'movie_cate_id',
            'c.type_name_ch' => 'type_name_ch',
        ];

        $list = $this->alias("m")
            ->field($field)
            ->join("__MOVIE_CATE__ c","m.movie_cate_id = c.movie_cate_id","left")
            ->order("m.add_time desc")
            ->where($where)
            ->paginate(10)
            ->each(function($item, $key){
                $item->add_time = date("Y-m-d H:i",$item->add_time);
                if ($item->movie_type == 0) {
                    $item->movie_type = '--';
                } elseif ($item->movie_type == 1) {
                    $item->movie_type = '热门强推';
                } else {
                    $item->movie_type = '--';
                }
            });
        return $list;
    }

    /**
     * 删除
     */
    public function del($movie_id)
    {
        if ($movie_id > 0) {
            $result = $this->where(["movie_id"=>$movie_id])->delete();
            return $result;
        }
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $data['add_time'] = time();
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询简介
     */
    public function findMovie($movie_id)
    {
        $manage = $this->where(['movie_id'=>$movie_id])->find();
        return $manage->toArray();
    }

    /**
     * 修改
     */
    public function upd($movie_id,$data)
    {
        $result = $this->save($data,['movie_id' => $movie_id]);
        return $result;
    }
}
