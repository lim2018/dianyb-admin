<?php
namespace app\admin\model;

use \think\Model;
class Group extends Model
{
    /**
     * 查询
     */
	public function sel()
    {
        $list = $this->where(['is_del' => 0])->order("group_id asc")->select();
        return collection($list)->toArray();
    }

    /**
     * 删除
     */
    public function del($group_id)
    {
        if ($group_id > 0) {
            $result = $this->save(['is_del' => 1],['group_id' => $group_id]);
            return $result;
        }
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $data['auth'] = implode(",",$data['auth']);
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询分组信息
     */
    public function findGroup($group_id)
    {
        $group = $this->where(['group_id'=>$group_id])->find();
        return $group->toArray();
    }

    /**
     * 修改
     */
    public function upd($group_id,$data)
    {
        $data['auth'] = implode(",",$data['auth']);
        $result = $this->save($data,['group_id' => $group_id]);
        return $result;
    }
}
