<?php
namespace app\admin\model;
use \think\Model;
class Setup extends Model
{
    /**
     * 设置
     */
    public function findsetup($setup_id)
    {
        $setup = $this->where(['setup_id'=>$setup_id])->find();
        return $setup->toArray();
    }

    /**
     * 修改
     */
    public function upd($setup_id,$data)
    {
        $result = $this->save($data,['setup_id' => $setup_id]);
        return $result;
    }
}
