<?php
namespace app\admin\model;

use \think\Model;
class Member extends Model
{
    /**
     * 查询
     */
	public function sel($phone_num = "")
    {
        $where['is_del'] = 0;
        if (!empty($phone_num)) {
            $where['phone_num'] = ["like","%$phone_num%"];
        }
        $list = $this->where($where)
            ->order('add_time desc')
            ->paginate(15)
            ->each(function($item, $key){
                $item->add_time = date("Y-m-d H:i",$item->add_time);
                if ($item->sex == 1) {
                    $item->sex = '男';
                } else {
                    $item->sex = '女';
                }
            });
        return $list;
    }

    /**
     * 删除
     */
    public function del($member_id)
    {
        if ($member_id > 0) {
            $result = $this->save(['is_del' => 1],['member_id' => $member_id]);
            return $result;
        }
    }
}
