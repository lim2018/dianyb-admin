<?php
namespace app\admin\model;

use \think\Model;
class Cate extends Model
{
    /**
     * 查询
     */
	public function sel()
    {
        $list = $this->where(['is_del' => 0])->order("cate_id asc")->select();
        return collection($list)->toArray();
    }

    /**
     * 删除
     */
    public function del($cate_id)
    {
        if ($cate_id > 0) {
            $result = $this->save(['is_del' => 1],['cate_id' => $cate_id]);
            return $result;
        }
    }

    /**
     * 查找子分类
     */
    public function selChildren($cate_id)
    {
        $cate = $this->where(['pid' => $cate_id])->select();
        return $cate;
    }

    /**
     * 新增
     */
    public function add($data)
    {
        $this->data($data);
        $result = $this->save();
        return $result;
    }

    /**
     * 查询分组信息
     */
    public function findcate($cate_id)
    {
        $cate = $this->where(['cate_id'=>$cate_id])->find();
        return $cate->toArray();
    }

    /**
     * 修改
     */
    public function upd($cate_id,$data)
    {
        $result = $this->save($data,['cate_id' => $cate_id]);
        return $result;
    }
}
