<?php
namespace app\admin\model;

use \think\Model;
class Node extends Model
{
    //设置数据返回类型
    protected $resultSetType = 'collection';

    /**
     * 查找菜单节点
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
	public function showNode()
    {
        $node = $this->where(['isshow'=>0])->order('sort asc')->select();
        return $node;
    }
}
