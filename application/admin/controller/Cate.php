<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Cate as CateModel;
class Cate extends Base
{
    /**
     * 标签列表
     * @return mixed
     */
    public function index()
    {
        $CateModel = new CateModel();
        $list = $CateModel->sel();
        $list = $this->cateSort($list);
        $this->assign([
            'list'  => $list
        ]);
        return $this->fetch();
    }

    /**
     * 按规则排序分类
     * @param $list
     * @return array
     */
    protected function cateSort($list)
    {
        $cates = array();
        //先找出顶级
        foreach ($list as $k => $val) {
            if($val['pid'] == 0) {
                $cates[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子
        foreach ($cates as $k => $val) {
            foreach ($list as $key => $value) {
                if($value['pid'] == $val['cate_id']) {
                    $value['cate_name'] = "└".$value['cate_name'];
                    $cates[$k]['children'][] = $value;
                }
            }
        }
        return $cates;
    }

    /**
     * 新增
     * @return mixed
     */
    public function add()
    {
        $CateModel = new CateModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['cate_name','require|length:2,16|unique:cate','分类名称必填|分类名称中英文2~16位|分类名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $CateModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            $pid = input("param.pid");
            $level = input("param.level");
            $level_name = input("param.level_name");
            $this->assign([
                'pid'  => $pid,
                'level' => $level,
                'level_name' => $level_name
            ]);
            return $this->fetch();
        }
    }

    /**
     * 编辑
     * @return mixed
     */
    public function upd()
    {
        $CateModel = new CateModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['cate_name','require|length:2,16|unique:cate','分类名称必填|分类名称中英文2~16位|分类名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $CateModel->upd($data['cate_id'],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $pid = input("param.pid");
            $level = input("param.level");
            $level_name = input("param.level_name");
            $cate = $CateModel->findCate(input('param.cate_id'));
            $this->assign([
                'pid'  => $pid,
                'level' => $level,
                'level_name' => $level_name,
                'cate' => $cate
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $CateModel = new CateModel();
        $cate_id = input('param.cate_id');
        $cate = $CateModel->selChildren($cate_id);
        if (!$cate) {
            $result = $CateModel->del($cate_id);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'删除成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'删除失败']);
            }
        } else {
            return json(['data'=>'','code'=>1,'message'=>'此分类下有子分类，不能直接删除']);
        }
    }

    /**
    * 按规则排序节点
    * @param $node
    * @return array
    */
    protected function nodeSort($node)
    {
        $cates = array();
        //先找出顶级菜单
        foreach ($node as $k => $val) {
            if($val['pid'] == 0) {
                $cates[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子菜单
        foreach ($cates as $k => $val) {
            foreach ($node as $key => $value) {
                if($value['pid'] == $val['node_id']) {
                    $cates[$k]['children'][] = $value;
                }
            }
        }
        return $cates;
    }
}
