<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Setup as SetupModel;
class Setup extends Base
{
    /**
     * 网站设置
     * @return mixed
     */
    public function index()
    {
        $SetupModel = new SetupModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['title','require|length:2,16','网站标题必填|网站标题中英文2~16位'],
                ['copyright','max:50','版权说明在50个字符以内'],
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $SetupModel->upd(1,$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $setup = $SetupModel->findSetup(1);
            $this->assign([
                'setup' => $setup
            ]);
            return $this->fetch();
        }
    }
}
