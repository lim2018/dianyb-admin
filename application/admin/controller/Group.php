<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Group as GroupModel;
class Group extends Base
{
    /**
     * 分组列表
     * @return mixed
     */
    public function index()
    {
        $GroupModel = new GroupModel();
        $list = $GroupModel->sel();
        $this->assign([
            'list'  => $list
        ]);
        return $this->fetch();
    }

    /**
     * 新增
     * @return mixed
     */
    public function add()
    {
        $GroupModel = new GroupModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['name','require|chs|length:2,8|unique:group','分组名称必填|分组名称中文2~8位|分组名称中文2~8位|分组名称已存在'],
                ['remark','max:20','备注不超过20个字符']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $GroupModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            $node = Db::name('node')->order('sort,name asc')->select();
            $node = $this->nodeSort($node);
            $this->assign([
                'node'  => $node
            ]);
            return $this->fetch();
        }

    }

    /**
     * 编辑
     * @return mixed
     */
    public function upd()
    {
        $GroupModel = new GroupModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['name','require|chs|length:2,8|unique:group','分组名称必填|分组名称中文2~8位|分组名称中文2~8位|分组名称已存在'],
                ['remark','max:20','备注不超过20个字符']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $GroupModel->upd($data['group_id'],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $group = $GroupModel->findGroup(input('param.group_id'));
            $node = Db::name('node')->order('sort,name asc')->select();
            $node = $this->nodeSort($node);
            $this->assign([
                'group' => $group,
                'node'  => $node
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $GroupModel = new GroupModel();
        $group_id = input('param.group_id');
        $result = $GroupModel->del($group_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }

    /**
    * 按规则排序节点
    * @param $node
    * @return array
    */
    protected function nodeSort($node)
    {
        $nodes = array();
        //先找出顶级菜单
        foreach ($node as $k => $val) {
            if($val['pid'] == 0) {
                $nodes[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子菜单
        foreach ($nodes as $k => $val) {
            foreach ($node as $key => $value) {
                if($value['pid'] == $val['node_id']) {
                    $nodes[$k]['children'][] = $value;
                }
            }
        }
        return $nodes;
    }
}
