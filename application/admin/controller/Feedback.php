<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Feedback as FeedbackModel;
class Feedback extends Base
{
    /**
     * 反馈列表
     * @return mixed
     */
    public function index()
    {
        $FeedbackModel = new FeedbackModel();
        $phone_num = input('param.phone_num');
        $list = $FeedbackModel->sel($phone_num);
        $page = $list->render();
        $this->assign([
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }
}
