<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Label as LabelModel;
class Label extends Base
{
    /**
     * 标签列表
     * @return mixed
     */
    public function index()
    {
        $LabelModel = new LabelModel();
        $list = $LabelModel->sel();
        $this->assign([
            'list'  => $list
        ]);
        return $this->fetch();
    }

    /**
     * 新增
     * @return mixed
     */
    public function add()
    {
        $LabelModel = new LabelModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['label_name','require|length:2,16|unique:Label','标签名称必填|标签名称中英文2~16位|标签名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $LabelModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            return $this->fetch();
        }
    }

    /**
     * 编辑
     * @return mixed
     */
    public function upd()
    {
        $LabelModel = new LabelModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['label_name','require|length:2,16|unique:Label','标签名称必填|标签名称中英文2~16位|标签名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $LabelModel->upd($data['label_id'],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $label = $LabelModel->findLabel(input('param.label_id'));
            $this->assign([
                'label' => $label
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $LabelModel = new LabelModel();
        $label_id = input('param.label_id');
        $result = $LabelModel->del($label_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }

    /**
    * 按规则排序节点
    * @param $node
    * @return array
    */
    protected function nodeSort($node)
    {
        $nodes = array();
        //先找出顶级菜单
        foreach ($node as $k => $val) {
            if($val['pid'] == 0) {
                $nodes[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子菜单
        foreach ($nodes as $k => $val) {
            foreach ($node as $key => $value) {
                if($value['pid'] == $val['node_id']) {
                    $nodes[$k]['children'][] = $value;
                }
            }
        }
        return $nodes;
    }
}
