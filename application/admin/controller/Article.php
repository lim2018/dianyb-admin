<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Article as ArticleModel;
use app\admin\model\Cate as CateModel;
use app\admin\model\Label as LabelModel;
class Article extends Base
{
    /**
     * 文章列表
     * @return mixed
     */
    public function index()
    {
        $ArticleModel = new ArticleModel();
        $art_title = input('param.art_title');
        $list = $ArticleModel->sel($art_title);
        $page = $list->render();
        $this->assign([
            'art_title' => input('param.art_title'),
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }

    /**
     * 查询详情
     */
    public function content()
    {
        $ArticleModel = new ArticleModel();
        $article_id = input('param.article_id');
        $article = $ArticleModel->findArt($article_id);
        $this->assign([
            'article' => $article
        ]);
        return $this->fetch();
    }

    /**
     * 文章新增
     */
    public function add()
    {
        $ArticleModel = new ArticleModel();
        $CateModel = new CateModel();
        $LabelModel = new LabelModel();
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['art_title','require|max:50|unique:article','文章标题必填|文章标题不能超过50字|文章标题已存在'],
                ['keyword','max:50','关键字不能超过50字'],
                ['describe','max:120','描述不能超过120字'],
                ['art_content','max:10000','内容不能超过10000字'],
            ]);
            $data = input('post.');
            $data['state'] = input('param.state');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $ArticleModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            $cate = $this->cateSort($CateModel->sel());
            $label = $LabelModel->sel();
            $this->assign([
                'cate'  => $cate,
                'label' => $label
            ]);
            return $this->fetch();
        }
    }

    /**
     * 文章编辑
     */
    public function upd()
    {
        $ArticleModel = new ArticleModel();
        $CateModel = new CateModel();
        $LabelModel = new LabelModel();
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['art_title','require|max:50|unique:article','文章标题必填|文章标题不能超过50字|文章标题已存在'],
                ['keyword','max:50','关键字不能超过50字'],
                ['describe','max:120','描述不能超过120字'],
                ['art_content','max:10000','内容不能超过10000字'],
            ]);
            $data = input('post.');
            $data['state'] = input('param.state');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $ArticleModel->upd($data['article_id'],$data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $article_id = input('param.article_id');
            $article = $ArticleModel->findArt($article_id);
            $cate = $this->cateSort($CateModel->sel());
            $label = $LabelModel->sel();
            $this->assign([
                'article' => $article,
                'cate'  => $cate,
                'label' => $label
            ]);
            return $this->fetch();
        }
    }

    /**
     * 按规则排序分类
     * @param $list
     * @return array
     */
    protected function cateSort($list)
    {
        $cates = array();
        //先找出顶级
        foreach ($list as $k => $val) {
            if($val['pid'] == 0) {
                $cates[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子
        foreach ($cates as $k => $val) {
            foreach ($list as $key => $value) {
                if($value['pid'] == $val['cate_id']) {
                    $value['cate_name'] = "└".$value['cate_name'];
                    $cates[$k]['children'][] = $value;
                }
            }
        }
        return $cates;
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $ArticleModel = new ArticleModel();
        $article_id = input('param.article_id');
        $result = $ArticleModel->del($article_id);
        if ($result >= 0) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
