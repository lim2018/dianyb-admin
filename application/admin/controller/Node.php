<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Validate;
class Node extends Controller
{
    /**
     * 节点列表
     * @return mixed
     */
    public function index()
    {
        $node = Db::name('node')->order('sort,name asc')->select();
        $node = $this->nodeSort($node);
        $this->assign([
            'node'  => $node
        ]);
        return $this->fetch();
    }

    /**
     * 新增节点
     * @return mixed
     */
    public function add()
    {
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['title','require|length:2,8','节点名称必填|节点名称在2~8字之间'],
                ['remark','max:20','备注最多20字']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = Db::name('node')->insert($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'添加成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'添加失败']);
            }
        } else {
            //页面显示
            $pid = input('param.pid');
            $parent = input('param.parent');
            $this->assign([
                'pid'  => $pid,
                'parent' => $parent
            ]);
            return $this->fetch();
        }
    }


    /**
     * 编辑节点
     * @return mixed
     */
    public function upd()
    {
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['title','require|length:2,8','节点名称必填|节点名称在2~8字之间'],
                ['remark','max:20','备注最多20字']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = Db::name('node')->update($data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            //页面显示
            $pid = input('param.pid');
            $node_id = input('param.node_id');
            $parent = input('param.parent');
            $node = Db::name('node')->where(['node_id' => $node_id])->find();
            $this->assign([
                'pid'  => $pid,
                'node'  => $node,
                'parent' => $parent
            ]);
            return $this->fetch();
        }
    }

    /**
     * 按规则排序节点
     * @param $node
     * @return array
     */
    protected function nodeSort($node)
    {
        $nodes = array();
        //先找出顶级菜单
        foreach ($node as $k => $val) {
            if($val['pid'] == 0) {
                $nodes[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子菜单
        foreach ($nodes as $k => $val) {
            foreach ($node as $key => $value) {
                if($value['pid'] == $val['node_id']) {
                    $value['title'] = "└".$value['title'];
                    $nodes[$k]['children'][] = $value;
                }
            }
        }
        return $nodes;
    }

    /**
     * 删除节点
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function del()
    {
        $node_id = input('post.node_id');
        $node = Db::name('node')->where(['pid' => $node_id])->select();
        //判断有没有子节点 ，没有则删除
        if (!$node) {
            $result = Db::name('node')->where(['node_id' => $node_id])->delete();
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'删除成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'删除失败']);
            }
        } else {
            return json(['data'=>'','code'=>1,'message'=>'此节点下有子节点，不能直接删除']);
        }
    }
}
