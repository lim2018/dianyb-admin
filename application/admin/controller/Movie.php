<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Movie as MovieModel;
use app\admin\model\MovieCate as MovieCateModel;
use app\admin\service\Upload;
class Movie extends Base
{
    /**
     * 文章列表
     * @return mixed
     */
    public function index()
    {
        $MovieModel = new MovieModel();
        $movie_title = input('param.movie_title');
        $list = $MovieModel->sel($movie_title);
        $page = $list->render();
        $this->assign([
            'movie_title' => input('param.movie_title'),
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }

    /**
     * 查询简介
     */
    public function content()
    {
        $MovieModel = new MovieModel();
        $movie_id= input('param.movie_id');
        $movie = $MovieModel->findMovie($movie_id);
        $this->assign([
            'movie' => $movie
        ]);
        return $this->fetch();
    }

    /**
     * 播放
     */
    public function play()
    {
        $MovieModel = new MovieModel();
        $movie_id= input('param.movie_id');
        $movie = $MovieModel->findMovie($movie_id);
        $this->assign([
            'movie' => $movie
        ]);
        return $this->fetch();
    }

    /**
     * 电影新增
     */
    public function add()
    {
        // 实例化模型对象
        $MovieModel = new MovieModel();
        $MovieCateModel = new MovieCateModel();
        // 提交方式判断
        if (request()->isPost()) {
            $file = request()->file('file');
            if (!$file) {
                return json(['data'=>'','code'=>1,'message'=>'封面必填']);
            }
            // 调用图片上传
            $upload = Upload::singleUpload($file,1,1);
            $face_pic = '';
            if (isset($upload['message']) && $upload['message'] == 'ok') {
                $face_pic = $upload['savename'];
            }
            //验证数据
            $validate = new Validate([
                ['movie_title','require|max:30|unique:movie','电影标题必填|文章标题不能超过30字|电影标题已存在'],
                ['actor','require|max:50','演员标题必填|演员不能超过50字'],
                ['intro','require|max:1000','简介必填|简介不能超过1000字'],
                ['score','require|number|<=:5.0','评分必填|评分必须是数字|评分上限5.0分'],
                ['movie_path','require','播放地址必填']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $data['face_pic'] = $face_pic;
            $result = $MovieModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            $cate = $MovieCateModel->sel();
            $this->assign([
                'cate'  => $cate
            ]);
            return $this->fetch();
        }
    }

    /**
     * 电影编辑
     */
    public function upd()
    {
        // 实例化模型对象
        $MovieModel = new MovieModel();
        $MovieCateModel = new MovieCateModel();
        // 判断提交方式
        if (request()->isPost()) {
            $file = request()->file('file');
            if ($file) {
                // 调用图片上传
                $upload = Upload::singleUpload($file,1,1);
                $face_pic = '';
                if (isset($upload['message']) && $upload['message'] == 'ok') {
                    $face_pic = $upload['savename'];
                }
            }
            //验证数据
            $validate = new Validate([
                ['movie_title','require|max:30|unique:movie','电影标题必填|文章标题不能超过30字|电影标题已存在'],
                ['actor','require|max:50','演员标题必填|演员不能超过50字'],
                ['intro','require|max:1000','简介必填|简介不能超过1000字'],
                ['score','require|number|<=:5.0','评分必填|评分必须是数字|评分上限5.0分'],
                ['movie_path','require','播放地址必填']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            if (isset($face_pic) && $face_pic != '') {
                $data['face_pic'] = $face_pic;
            }
            $result = $MovieModel->upd($data['movie_id'],$data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $movie = $MovieModel->findMovie(input('param.movie_id'));
            $cate = $MovieCateModel->sel();
            $this->assign([
                'movie' => $movie,
                'cate'  => $cate,
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $MovieModel = new MovieModel();
        $movie_id = input('param.movie_id');
        $result = $MovieModel->del($movie_id);
        if ($result >= 0) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
