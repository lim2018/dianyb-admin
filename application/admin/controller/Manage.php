<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Manage as ManageModel;
class Manage extends Base
{
    /**
     * 管理员列表
     * @return mixed
     */
    public function index()
    {
        $ManageModel = new ManageModel();
        $username = input('param.username');
        $list = $ManageModel->sel($username);
        $page = $list->render();
        $this->assign([
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }

    /**
     * 新增
     * @return mixed
     */
    public function add()
    {
        $ManageModel = new ManageModel();
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['username','require|alphaDash|length:4,16|unique:manage','用户名称必填|用户名称英文数字组合4~16位|用户名称英文数字组合4~16位|用户名已存在'],
                ['group_id','gt:0','必须选择一个分组'],
                ['password','require|alphaDash|length:6,20','密码必填|密码为英文数字组合6~20位|密码为英文数字组合6~20位'],
                ['email','require|email','邮箱必填|邮箱格式必须正确'],
                ['real_name','require|chs|length:2,8','真实姓名必填|真实姓名必须为中文2~8位|真实姓名必须为中文2~8位'],
                ['id_card','require|alphaNum|length:18','身份证必填|请检查身份证格式|请检查身份证格式'],
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $ManageModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            $group = Db::name("group")->select();
            $this->assign([
                'group'  => $group
            ]);
            return $this->fetch();
        }
    }

    /**
     * 编辑
     * @return mixed
     */
    public function upd()
    {
        $ManageModel = new ManageModel();
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['username','require|alphaDash|length:4,16|unique:manage','用户名称必填|用户名称英文数字组合4~16位|用户名称英文数字组合4~16位|用户名已存在'],
                ['group_id','gt:0','必须选择一个分组'],
                ['password','alphaDash|length:6,20','密码为英文数字组合6~20位|密码为英文数字组合6~20位'],
                ['email','require|email','邮箱必填|邮箱格式必须正确'],
                ['real_name','require|chs|length:2,8','真实姓名必填|真实姓名必须为中文2~8位|真实姓名必须为中文2~8位'],
                ['id_card','require|alphaNum|length:18','身份证必填|请检查身份证格式|请检查身份证格式'],
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $ManageModel->upd($data['manage_id'],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }

        } else {
            $manage = $ManageModel->findUser(input('param.manage_id'));
            $group = Db::name("group")->select();
            $this->assign([
                'group'  => $group,
                'manage' => $manage
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $ManageModel = new ManageModel();
        $manage_id = input('param.manage_id');
        $result = $ManageModel->del($manage_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
