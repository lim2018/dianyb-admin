<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Comment as CommentModel;
class Comment extends Base
{
    /**
     * 评论列表
     * @return mixed
     */
    public function index()
    {
        $CommentModel = new CommentModel();
        $type = input('param.type');
        $keyword = input('param.keyword');
        $list = $CommentModel->sel($type,$keyword);
        $page = $list->render();
        $this->assign([
            'keyword' => $keyword,
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }


    /**
     * 屏蔽
     * @return \think\response\Json
     */
    public function shield()
    {
        $CommentModel = new CommentModel();
        $comment_id = input('param.comment_id');
        $result = $CommentModel->shield($comment_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'屏蔽成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'屏蔽失败']);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $CommentModel = new CommentModel();
        $comment_id = input('param.comment_id');
        $result = $CommentModel->del($comment_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
