<?php
namespace app\admin\controller;

use think\Controller;

class Upload extends Controller
{
    /**
     * 构造函数
     */
    public function _initialize()
    {
        // 允许所有域名访问
        header('Access-Control-Allow-Origin:*');
    }
    /**
     * 单文件上传
     */
    public function singleUpload(){
        // 获取post
        if (input('post.type') == 1) {
            $type = 'image';
        } elseif (input('post.type') == 2) {
            $type = 'video';
        } else {
            $type = 'temp';
        }
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads'.DS.$type);
            if($info){
                // 成功上传后 获取上传信息
                $data = [
                    'savename' => $type.'/'.str_replace('\\', '/',  $info->getSaveName()),
                    'filename' => $info->getFilename(),
                ];
                return json(['data'=>$data,'code'=>0,'message'=>'上传成功']);
            }else{
                // 上传失败获取错误信息
                return json(['data'=>'','code'=>1,'message'=>$file->getError()]);
            }
        }
    }

    /**
     * 多文件上传
     */
    public function multitudeUpload()
    {
        // 获取post
        if (input('post.type') == 1) {
            $type = 'image';
        } elseif (input('post.type') == 2) {
            $type = 'video';
        } else {
            $type = 'temp';
        }
        // 获取表单上传文件
        $files = request()->file('file');
        $tempArr = [];
        foreach($files as $key=>$file){
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads'.DS.$type);
            if($info){
                // 成功上传后 获取上传信息
                $data = [
                    'savename' => $type.'/'.str_replace('\\', '/',  $info->getSaveName()),
                    'filename' => $info->getFilename(),
                    'message' => '成功'
                ];
                $tempArr[$key]['info'] = $data;
            }else{
                // 上传失败获取错误信息
                $data = [
                    'savename' => $type.'/'.str_replace('\\', '/',  $info->getSaveName()),
                    'filename' => $info->getFilename(),
                    'message' => $file->getError()
                ];
                $tempArr[$key]['info'] = $data;
            }
        }
        return json(['data'=>$tempArr,'code'=>0,'message'=>'上传成功']);
    }
}
