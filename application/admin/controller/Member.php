<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\Member as MemberModel;
class Member extends Base
{
    /**
     * 会员列表
     * @return mixed
     */
    public function index()
    {
        $MemberModel = new MemberModel();
        $phone_num = input('param.phone_num');
        $list = $MemberModel->sel($phone_num);
        $page = $list->render();
        $this->assign([
            'list'  => $list,
            'page' => $page
        ]);
        return $this->fetch();
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $MemberModel = new MemberModel();
        $member_id = input('param.member_id');
        $result = $MemberModel->del($member_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
