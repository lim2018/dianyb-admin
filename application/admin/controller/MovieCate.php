<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use app\admin\model\MovieCate as MovieCateModel;
class MovieCate extends Base
{
    /**
     * 电影分类列表
     * @return mixed
     */
    public function index()
    {
        $MovieCateModel = new MovieCateModel();
        $list = $MovieCateModel->sel();
        $this->assign([
            'list'  => $list
        ]);
        return $this->fetch();
    }

    /**
     * 新增
     * @return mixed
     */
    public function add()
    {
        $MovieCateModel = new MovieCateModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['type_name_ch','require|chs|length:2,8|unique:movie_cate','电影分类名称必填|电影分类名称必须为中文|电影分类名称中文2~8位|电影分类名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $MovieCateModel->add($data);
            if ($result) {
                return json(['data'=>'','code'=>0,'message'=>'新增成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'新增失败']);
            }
        } else {
            return $this->fetch();
        }
    }

    /**
     * 编辑
     * @return mixed
     */
    public function upd()
    {
        $MovieCateModel = new MovieCateModel();
        //表单提交
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['type_name_ch','require|chs|length:2,8|unique:movie_cate','电影分类名称必填|电影分类名称必须为中文|电影分类名称中文2~8位|电影分类名称已存在']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $result = $MovieCateModel->upd($data['movie_cate_id'],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            $movie_cate = $MovieCateModel->findMovieCate(input('param.movie_cate_id'));
            $this->assign([
                'movie_cate' => $movie_cate
            ]);
            return $this->fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del()
    {
        $MovieCateModel = new MovieCateModel();
        $label_id = input('param.movie_cate_id');
        $result = $MovieCateModel->del($label_id);
        if ($result) {
            return json(['data'=>'','code'=>0,'message'=>'删除成功']);
        } else {
            return json(['data'=>'','code'=>1,'message'=>'删除失败']);
        }
    }
}
