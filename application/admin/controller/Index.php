<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use app\admin\model\Node as NodeModel;
use app\admin\model\Log as LogModel;
class Index extends Base
{
    /**
     * 后台主页
     * @return mixed
     */
    public function index()
    {
        $node = $this->nodeSort($this->getAuthNode('show'));
        $conf_node_list = Config::get('node_list');
        $this->assign([
            'node'  => $node,
            'conf_node_list' => $conf_node_list
        ]);
        return $this->fetch();
    }

    /**
     * 欢迎页
     * @return mixed
     */
    public function welcome()
    {
        $LogModel = new LogModel();
        $list = $LogModel->selByUser("login",Session::get("admin")["manage_id"],10);
        $count = $LogModel->logNum();
        $this->assign([
            'list'  => $list,
            'count' =>$count
        ]);
        return $this->fetch();
    }

}
