<?php
namespace app\admin\controller;
use think\Config;
use think\Controller;
use think\Validate;
use think\Db;
use think\Session;
use app\admin\model\Manage as ManageModel;
class Userinfo extends Base
{
    /**
     * 个人信息
     */
    public function index()
    {
        $ManageModel = new ManageModel();
        $admin = Session::get("admin");
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['username','require|alphaDash|length:4,16|unique:manage','用户名称必填|用户名称英文数字组合4~16位|用户名称英文数字组合4~16位|用户名已存在'],
                ['group_id','gt:0','必须选择一个分组'],
                ['email','require|email','邮箱必填|邮箱格式必须正确'],
                ['real_name','require|chs|length:2,8','真实姓名必填|真实姓名必须为中文2~8位|真实姓名必须为中文2~8位'],
                ['id_card','require|alphaNum|length:18','身份证必填|请检查身份证格式|请检查身份证格式'],
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            $data['password'] = '';
            $result = $ManageModel->upd($admin["manage_id"],$data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }

        } else {
            $manage = $ManageModel->findUser($admin["manage_id"]);
            $group = Db::name("group")->select();
            $this->assign([
                'group'  => $group,
                'manage' => $manage
            ]);
            return $this->fetch();
        }
    }

    /**
     * 修改密码
     */
    public function pwd()
    {
        $ManageModel = new ManageModel();
        $admin = Session::get("admin");
        if (request()->isPost()) {
            //验证数据
            $validate = new Validate([
                ['password1','require|alphaDash|length:6,20','旧密码必填|旧密码为英文数字组合6~20位|旧密码为英文数字组合6~20位'],
                ['password2','require|alphaDash|length:6,20','新密码必填|新密码为英文数字组合6~20位|新密码为英文数字组合6~20位']
            ]);
            $data = input('post.');
            if (!$validate->check($data)) {
                $message = $validate->getError();
                return json(['data'=>'','code'=>1,'message'=>$message]);
            }
            //判断旧密码是否正确
            if (md5($data['password1']) != $admin['password']) {
                return json(['data'=>'','code'=>1,'message'=>'旧密码错误']);
            }
            //更新新密码
            $new_data = ['password'=>md5($data['password2'])];
            $result = $ManageModel->upd($admin["manage_id"],$new_data);
            if ($result >= 0) {
                return json(['data'=>'','code'=>0,'message'=>'更新成功']);
            } else {
                return json(['data'=>'','code'=>1,'message'=>'更新失败']);
            }
        } else {
            return $this->fetch();
        }
    }
}
