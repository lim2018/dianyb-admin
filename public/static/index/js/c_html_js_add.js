var zbp = new ZBP({
    bloghost: "https://www.songhaifeng.com/",
    ajaxurl: "https://www.songhaifeng.com/zb_system/cmd.php?act=ajax&src=",
    cookiepath: "/",
    lang: {
        error: {
            72: "名称不能为空或格式不正确",
            29: "邮箱格式不正确，可能过长或为空",
            46: "评论内容不能为空或过长"
        }
    }
});

var bloghost = zbp.options.bloghost;
var cookiespath = zbp.options.cookiepath;
var ajaxurl = zbp.options.ajaxurl;
var lang_comment_name_error = zbp.options.lang.error[72];
var lang_comment_email_error = zbp.options.lang.error[29];
var lang_comment_content_error = zbp.options.lang.error[46];

$(function () {

    zbp.cookie.set("timezone", (new Date().getTimezoneOffset()/60)*(-1));
    var $cpLogin = $(".cp-login").find("a");
    var $cpVrs = $(".cp-vrs").find("a");
    var $addinfo = zbp.cookie.get("addinfo");
    if (!$addinfo){
        return ;
    }
    $addinfo = JSON.parse($addinfo);

    if ($addinfo.chkadmin){
        $(".cp-hello").html("欢迎 " + $addinfo.useralias + " (" + $addinfo.levelname  + ")");
        if ($cpLogin.length == 1 && $cpLogin.html().indexOf("[") > -1) {
            $cpLogin.html("[后台管理]");
        } else {
            $cpLogin.html("后台管理");
        }
    }

    if($addinfo.chkarticle){
        if ($cpLogin.length == 1 && $cpVrs.html().indexOf("[") > -1) {
            $cpVrs.html("[新建文章]");
        } else {
            $cpVrs.html("新建文章");
        }
        $cpVrs.attr("href", zbp.options.bloghost + "zb_system/cmd.php?act=ArticleEdt");
    }

});
		upyunbloghost = 'https://res.songhaifeng.com';
		
	

	window._js = {sID:'#sidebar',fID: '.sidebarfollowed',sWD: '318',sTP: '0',bID: '#footer'};
	now = new Date();numnum = 66;
	$.fn.rTabs = function(options) { var defaultVal = { btnClass: '.j-tab-nav', conClass: '.j-tab-con', bind: 'hover', animation: '0', speed: 300, delay: 66, auto: true, autoSpeed: 3000 }; var obj = $.extend(defaultVal, options), evt = obj.bind, btn = $(this).find(obj.btnClass), con = $(this).find(obj.conClass), anim = obj.animation, conWidth = con.width(), conHeight = con.height(), len = con.children().length, sw = len * conWidth, sh = len * conHeight, i = 0, len, t, timer; return this.each(function() { function judgeAnim() { var w = i * conWidth, h = i * conHeight; btn.children().removeClass('current').eq(i).addClass('current'); switch (anim) { case '0': con.children().hide().eq(i).show(); break; case 'left': con.css({ position: 'absolute', width: sw }).children().css({ float: 'left', display: 'block' }).end().stop().animate({ left: -w }, obj.speed); break; case 'up': con.css({ position: 'absolute', height: sh }).children().css({ display: 'block' }).end().stop().animate({ top: -h }, obj.speed); break; case 'fadein': con.children().hide().eq(i).fadeIn(); break; } } if (evt == "hover") { btn.children().hover(function() { var j = $(this).index(); function s() { i = j; judgeAnim(); } timer = setTimeout(s, obj.delay); }, function() { clearTimeout(timer); }) } else { btn.children().bind(evt, function() { i = $(this).index(); judgeAnim(); }) } function startRun() { t = setInterval(function() { i++; if (i >= len) { switch (anim) { case 'left': con.stop().css({ left: conWidth }); break; case 'up': con.stop().css({ top: conHeight }); } i = 0; } judgeAnim(); }, obj.autoSpeed) } if (obj.auto) { $(this).hover(function() { clearInterval(t); }, function() { startRun(); }); startRun(); } }) }
	!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!1,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);UBB4ZBPFaceList = "dog|smile|laugh|laughing|shy|naughty|laughs|happy|decline|anger|cry|crys|chagrin|awkward|spit|torment|despise|bye|shutup|nose|poor|abuse|right|left|zan|rose";		

		var CanvasParticle = (function(){function getElementByTag(name){return document.getElementsByTagName(name);};function getELementById(id){return document.getElementById(id);};function canvasInit(canvasConfig){canvasConfig=canvasConfig||{};var html=getElementByTag("html")[0];var body=getElementByTag("body")[0];var canvasDiv=getELementById("canvas-particle");var canvasObj=document.createElement("canvas");var canvas={element:canvasObj,points:[],config:{vx:canvasConfig.vx||4,vy:canvasConfig.vy||4,height:canvasConfig.height||2,width:canvasConfig.width||2,count:canvasConfig.count||100,color:canvasConfig.color||"121, 162, 185",stroke:canvasConfig.stroke||"130,255,255",dist:canvasConfig.dist||6000,e_dist:canvasConfig.e_dist||20000,max_conn:10}};if(canvas.element.getContext("2d")){canvas.context=canvas.element.getContext("2d")}else{return null};body.style.padding="0";body.style.margin="0";body.appendChild(canvas.element);canvas.element.style="position: fixed; top: 0; left: 0; z-index: -1;";canvasSize(canvas.element);window.onresize=function(){canvasSize(canvas.element)};body.onmousemove=function(e){var event=e||window.event;canvas.mouse={x:event.clientX,y:event.clientY}};document.onmouseleave=function(){canvas.mouse=undefined};setInterval(function(){drawPoint(canvas)},40)};function canvasSize(canvas){canvas.width=window.innerWeight||document.documentElement.clientWidth||document.body.clientWidth;canvas.height=window.innerWeight||document.documentElement.clientHeight||document.body.clientHeight};function drawPoint(canvas){var context=canvas.context,point,dist;context.clearRect(0,0,canvas.element.width,canvas.element.height);context.beginPath();context.fillStyle="rgb("+canvas.config.color+")";for(var i=0,len=canvas.config.count;i<len;i++){if(canvas.points.length!=canvas.config.count){point={x:Math.floor(Math.random()*canvas.element.width),y:Math.floor(Math.random()*canvas.element.height),vx:canvas.config.vx/2-Math.random()*canvas.config.vx,vy:canvas.config.vy/2-Math.random()*canvas.config.vy}}else{point=borderPoint(canvas.points[i],canvas)};context.fillRect(point.x-canvas.config.width/2,point.y-canvas.config.height/2,canvas.config.width,canvas.config.height);canvas.points[i]=point};drawLine(context,canvas,canvas.mouse);context.closePath()};function borderPoint(point,canvas){var p=point;if(point.x<=0||point.x>=canvas.element.width){p.vx=-p.vx;p.x+=p.vx}else if(point.y<=0||point.y>=canvas.element.height){p.vy=-p.vy;p.y+=p.vy}else{p={x:p.x+p.vx,y:p.y+p.vy,vx:p.vx,vy:p.vy}};return p};function drawLine(context,canvas,mouse){context=context||canvas.context;for(var i=0,len=canvas.config.count;i<len;i++){canvas.points[i].max_conn=0;for(var j=0;j<len;j++){if(i!=j){dist=Math.round(canvas.points[i].x-canvas.points[j].x)*Math.round(canvas.points[i].x-canvas.points[j].x)+Math.round(canvas.points[i].y-canvas.points[j].y)*Math.round(canvas.points[i].y-canvas.points[j].y);if(dist<=canvas.config.dist&&canvas.points[i].max_conn<canvas.config.max_conn){canvas.points[i].max_conn++;context.lineWidth=0.5-dist/canvas.config.dist;context.strokeStyle="rgba("+canvas.config.stroke+","+(1-dist/canvas.config.dist)+")";context.beginPath();context.moveTo(canvas.points[i].x,canvas.points[i].y);context.lineTo(canvas.points[j].x,canvas.points[j].y);context.stroke()}}};if(mouse){dist=Math.round(canvas.points[i].x-mouse.x)*Math.round(canvas.points[i].x-mouse.x)+Math.round(canvas.points[i].y-mouse.y)*Math.round(canvas.points[i].y-mouse.y);if(dist>canvas.config.dist&&dist<=canvas.config.e_dist){canvas.points[i].x=canvas.points[i].x+(mouse.x-canvas.points[i].x)/20;canvas.points[i].y=canvas.points[i].y+(mouse.y-canvas.points[i].y)/20};if(dist<=canvas.config.e_dist){context.lineWidth=1;context.strokeStyle="rgba("+canvas.config.stroke+","+(1-dist/canvas.config.e_dist)+")";context.beginPath();context.moveTo(canvas.points[i].x,canvas.points[i].y);context.lineTo(mouse.x,mouse.y);context.stroke()}}}}return canvasInit;})();window.onload = function(){var config = {vx: 4,vy:  4,height: 2,width: 2,count: 100,color: "121, 162, 185",stroke: "100,200,180",dist: 6000,e_dist: 20000,max_conn: 10};CanvasParticle(config);};
document.writeln("<script src='https://www.songhaifeng.com/zb_users/plugin/UEditor/third-party/prism/prism.js' type='text/javascript'></script><link rel='stylesheet' type='text/css' href='https://www.songhaifeng.com/zb_users/plugin/UEditor/third-party/prism/prism.css'/>");$(function(){var compatibility={as3:"actionscript","c#":"csharp",delphi:"pascal",html:"markup",xml:"markup",vb:"basic",js:"javascript",plain:"markdown",pl:"perl",ps:"powershell"};var runFunction=function(doms,callback){doms.each(function(index,unwrappedDom){var dom=$(unwrappedDom);var codeDom=$("<code>");if(callback)callback(dom);var languageClass="prism-language-"+function(classObject){if(classObject===null)return"markdown";var className=classObject[1];return compatibility[className]?compatibility[className]:className}(dom.attr("class").match(/prism-language-([0-9a-zA-Z]+)/));codeDom.html(dom.html()).addClass("prism-line-numbers").addClass(languageClass);dom.html("").addClass(languageClass).append(codeDom)})};runFunction($("pre.prism-highlight"));runFunction($('pre[class*="brush:"]'),function(preDom){var original;if((original=preDom.attr("class").match(/brush:([a-zA-Z0-9\#]+);/))!==null){preDom.get(0).className="prism-highlight prism-language-"+original[1]}});Prism.highlightAll()});

var nobird_cache_ajaxurl = bloghost +"zb_users/plugin/Nobird_Cache/ajax.php";
function Nobird_Cache_LoadViewNums(prevalue,postid,callback){$.post(nobird_cache_ajaxurl,{"postid":postid,"add":0,"prevalue":prevalue},function(data){$("#nbcache" + postid+callback).html(data);});return false;}
function Nobird_Cache_AddViewNums(prevalue,postid,callback){$.post(nobird_cache_ajaxurl,{"postid":postid,"add":1,"prevalue":prevalue},function(data){$("#nbcache" + postid+callback).html(data);});return false;}
