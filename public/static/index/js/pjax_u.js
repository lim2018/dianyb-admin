/*! https://github.com/defunkt/jquery-pjax */
/*!
 * Copyright 2012, Chris Wanstrath
 * Released under the MIT License
 * https://github.com/defunkt/jquery-pjax
 */
(function($){
function fnPjax(selector, container, options) {
  options = optionsFor(container, options)
  return this.on('click.pjax', selector, function(event) {
    var opts = options
    if (!opts.container) {
      opts = $.extend({}, options)
      opts.container = $(this).attr('data-pjax')
    }
    handleClick(event, opts)
  })
}
function handleClick(event, container, options) {
  options = optionsFor(container, options)
  var link = event.currentTarget
  var $link = $(link)
  if (link.tagName.toUpperCase() !== 'A')
    throw "$.fn.pjax or $.pjax.click requires an anchor element"
  if ( event.which > 1 || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey )
    return
  if ( location.protocol !== link.protocol || location.hostname !== link.hostname )
    return
  if ( link.href.indexOf('#') > -1 && stripHash(link) == stripHash(location) )
    return
  if (event.isDefaultPrevented())
    return
  var defaults = {
    url: link.href,
    container: $link.attr('data-pjax'),
    target: link
  }
  var opts = $.extend({}, defaults, options)
  var clickEvent = $.Event('pjax:click')
  $link.trigger(clickEvent, [opts])
  if (!clickEvent.isDefaultPrevented()) {
    pjax(opts)
    event.preventDefault()
    $link.trigger('pjax:clicked', [opts])
  }
}
function handleSubmit(event, container, options) {
  options = optionsFor(container, options)
  var form = event.currentTarget
  var $form = $(form)
  if (form.tagName.toUpperCase() !== 'FORM')
    throw "$.pjax.submit requires a form element"
  var defaults = {
    type: ($form.attr('method') || 'GET').toUpperCase(),
    url: $form.attr('action'),
    container: $form.attr('data-pjax'),
    target: form
  }
  if (defaults.type !== 'GET' && window.FormData !== undefined) {
    defaults.data = new FormData(form)
    defaults.processData = false
    defaults.contentType = false
  } else {
    if ($form.find(':file').length) {
      return
    }
    defaults.data = $form.serializeArray()
  }
  pjax($.extend({}, defaults, options))
  event.preventDefault()
}
function pjax(options) {
  options = $.extend(true, {}, $.ajaxSettings, pjax.defaults, options)

  if ($.isFunction(options.url)) {
    options.url = options.url()
  }
  var hash = parseURL(options.url).hash
  var containerType = $.type(options.container)
  if (containerType !== 'string') {
    throw "expected string value for 'container' option; got " + containerType
  }
  var context = options.context = $(options.container)
  if (!context.length) {
    throw "the container selector '" + options.container + "' did not match anything"
  }
  if (!options.data) options.data = {}
  if ($.isArray(options.data)) {
    options.data.push({name: '_pjax', value: options.container})
  } else {
    options.data._pjax = options.container
  }
  function fire(type, args, props) {
    if (!props) props = {}
    props.relatedTarget = options.target
    var event = $.Event(type, props)
    context.trigger(event, args)
    return !event.isDefaultPrevented()
  }
  var timeoutTimer
  options.beforeSend = function(xhr, settings) {
    if (settings.type !== 'GET') {
      settings.timeout = 0
    }
    xhr.setRequestHeader('X-PJAX', 'true')
    xhr.setRequestHeader('X-PJAX-Container', options.container)
    if (!fire('pjax:beforeSend', [xhr, settings]))
      return false
    if (settings.timeout > 0) {
      timeoutTimer = setTimeout(function() {
        if (fire('pjax:timeout', [xhr, options]))
          xhr.abort('timeout')
      }, settings.timeout)
      settings.timeout = 0
    }
    var url = parseURL(settings.url)
    if (hash) url.hash = hash
    options.requestUrl = stripInternalParams(url)
  }
  options.complete = function(xhr, textStatus) {
    if (timeoutTimer)
      clearTimeout(timeoutTimer)
    fire('pjax:complete', [xhr, textStatus, options])
    fire('pjax:end', [xhr, options])
  }
  options.error = function(xhr, textStatus, errorThrown) {
    var container = extractContainer("", xhr, options)

    var allowed = fire('pjax:error', [xhr, textStatus, errorThrown, options])
    if (options.type == 'GET' && textStatus !== 'abort' && allowed) {
      locationReplace(container.url)
    }
  }
  options.success = function(data, status, xhr) {
    var previousState = pjax.state
    var currentVersion = typeof $.pjax.defaults.version === 'function' ?
      $.pjax.defaults.version() :
      $.pjax.defaults.version
    var latestVersion = xhr.getResponseHeader('X-PJAX-Version')
    var container = extractContainer(data, xhr, options)
    var url = parseURL(container.url)
    if (hash) {
      url.hash = hash
      container.url = url.href
    }
    if (currentVersion && latestVersion && currentVersion !== latestVersion) {
      locationReplace(container.url)
      return
    }
    if (!container.contents) {
      locationReplace(container.url)
      return
    }
    pjax.state = {
      id: options.id || uniqueId(),
      url: container.url,
      title: container.title,
      container: options.container,
      fragment: options.fragment,
      timeout: options.timeout
    }
    if (options.push || options.replace) {
      window.history.replaceState(pjax.state, container.title, container.url)
    }
    var blurFocus = $.contains(context, document.activeElement)
    if (blurFocus) {
      try {
        document.activeElement.blur()
      } catch (e) {}
    }
    if (container.title) document.title = container.title
    fire('pjax:beforeReplace', [container.contents, options], {
      state: pjax.state,
      previousState: previousState
    })
    context.html(container.contents)
    var autofocusEl = context.find('input[autofocus], textarea[autofocus]').last()[0]
    if (autofocusEl && document.activeElement !== autofocusEl) {
      autofocusEl.focus()
    }
    executeScriptTags(container.scripts)
    var scrollTo = options.scrollTo
    if (hash) {
      var name = decodeURIComponent(hash.slice(1))
      var target = document.getElementById(name) || document.getElementsByName(name)[0]
      if (target) scrollTo = $(target).offset().top
    }
    if (typeof scrollTo == 'number') $(window).scrollTop(scrollTo)

    fire('pjax:success', [data, status, xhr, options])
  }
  if (!pjax.state) {
    pjax.state = {
      id: uniqueId(),
      url: window.location.href,
      title: document.title,
      container: options.container,
      fragment: options.fragment,
      timeout: options.timeout
    }
    window.history.replaceState(pjax.state, document.title)
  }
  abortXHR(pjax.xhr)
  pjax.options = options
  var xhr = pjax.xhr = $.ajax(options)
  if (xhr.readyState > 0) {
    if (options.push && !options.replace) {
      cachePush(pjax.state.id, [options.container, cloneContents(context)])
      window.history.pushState(null, "", options.requestUrl)
    }
    fire('pjax:start', [xhr, options])
    fire('pjax:send', [xhr, options])
  }
  return pjax.xhr
}
function pjaxReload(container, options) {
  var defaults = {
    url: window.location.href,
    push: false,
    replace: true,
    scrollTo: false
  }
  return pjax($.extend(defaults, optionsFor(container, options)))
}
function locationReplace(url) {
  window.history.replaceState(null, "", pjax.state.url)
  window.location.replace(url)
}
var initialPop = true
var initialURL = window.location.href
var initialState = window.history.state
if (initialState && initialState.container) {
  pjax.state = initialState
}
if ('state' in window.history) {
  initialPop = false
}
function onPjaxPopstate(event) {
  if (!initialPop) {
    abortXHR(pjax.xhr)
  }
  var previousState = pjax.state
  var state = event.state
  var direction
  if (state && state.container) {
    if (initialPop && initialURL == state.url) return
    if (previousState) {
      if (previousState.id === state.id) return
      direction = previousState.id < state.id ? 'forward' : 'back'
    }
    var cache = cacheMapping[state.id] || []
    var containerSelector = cache[0] || state.container
    var container = $(containerSelector), contents = cache[1]
    if (container.length) {
      if (previousState) {
        cachePop(direction, previousState.id, [containerSelector, cloneContents(container)])
      }
      var popstateEvent = $.Event('pjax:popstate', {
        state: state,
        direction: direction
      })
      container.trigger(popstateEvent)
      var options = {
        id: state.id,
        url: state.url,
        container: containerSelector,
        push: false,
        fragment: state.fragment,
        timeout: state.timeout,
        scrollTo: false
      }
      if (contents) {
        container.trigger('pjax:start', [null, options])
        pjax.state = state
        if (state.title) document.title = state.title
        var beforeReplaceEvent = $.Event('pjax:beforeReplace', {
          state: state,
          previousState: previousState
        })
        container.trigger(beforeReplaceEvent, [contents, options])
        container.html(contents)
        container.trigger('pjax:end', [null, options])
      } else {
        pjax(options)
      }
      //container[0].offsetHeight
    } else {
      locationReplace(location.href)
    }
  }
  initialPop = false
}
function fallbackPjax(options) {
  var url = $.isFunction(options.url) ? options.url() : options.url,
      method = options.type ? options.type.toUpperCase() : 'GET'
  var form = $('<form>', {
    method: method === 'GET' ? 'GET' : 'POST',
    action: url,
    style: 'display:none'
  })
  if (method !== 'GET' && method !== 'POST') {
    form.append($('<input>', {
      type: 'hidden',
      name: '_method',
      value: method.toLowerCase()
    }))
  }
  var data = options.data
  if (typeof data === 'string') {
    $.each(data.split('&'), function(index, value) {
      var pair = value.split('=')
      form.append($('<input>', {type: 'hidden', name: pair[0], value: pair[1]}))
    })
  } else if ($.isArray(data)) {
    $.each(data, function(index, value) {
      form.append($('<input>', {type: 'hidden', name: value.name, value: value.value}))
    })
  } else if (typeof data === 'object') {
    var key
    for (key in data)
      form.append($('<input>', {type: 'hidden', name: key, value: data[key]}))
  }
  $(document.body).append(form)
  form.submit()
}
function abortXHR(xhr) {
  if ( xhr && xhr.readyState < 4) {
    xhr.onreadystatechange = $.noop
    xhr.abort()
  }
}
function uniqueId() {
  return (new Date).getTime()
}
function cloneContents(container) {
  var cloned = container.clone()
  cloned.find('script').each(function(){
    if (!this.src) $._data(this, 'globalEval', false)
  })
  return cloned.contents()
}
function stripInternalParams(url) {
  url.search = url.search.replace(/([?&])(_pjax|_)=[^&]*/g, '').replace(/^&/, '')
  return url.href.replace(/\?($|#)/, '$1')
}
function parseURL(url) {
  var a = document.createElement('a')
  a.href = url
  return a
}
function stripHash(location) {
  return location.href.replace(/#.*/, '')
}
function optionsFor(container, options) {
  if (container && options) {
    options = $.extend({}, options)
    options.container = container
    return options
  } else if ($.isPlainObject(container)) {
    return container
  } else {
    return {container: container}
  }
}
function findAll(elems, selector) {
  return elems.filter(selector).add(elems.find(selector))
}
function parseHTML(html) {
  return $.parseHTML(html, document, true)
}
function extractContainer(data, xhr, options) {
  var obj = {}, fullDocument = /<html/i.test(data)
  var serverUrl = xhr.getResponseHeader('X-PJAX-URL')
  obj.url = serverUrl ? stripInternalParams(parseURL(serverUrl)) : options.requestUrl
  var $head, $body
  if (fullDocument) {
    $body = $(parseHTML(data.match(/<body[^>]*>([\s\S.]*)<\/body>/i)[0]))
    var head = data.match(/<head[^>]*>([\s\S.]*)<\/head>/i)
    $head = head != null ? $(parseHTML(head[0])) : $body
  } else {
    $head = $body = $(parseHTML(data))
  }
  if ($body.length === 0)
    return obj
  obj.title = findAll($head, 'title').last().text()
  if (options.fragment) {
    var $fragment = $body
    if (options.fragment !== 'body') {
      $fragment = findAll($fragment, options.fragment).first()
    }
    if ($fragment.length) {
      obj.contents = options.fragment === 'body' ? $fragment : $fragment.contents()
      if (!obj.title)
        obj.title = $fragment.attr('title') || $fragment.data('title')
    }
  } else if (!fullDocument) {
    obj.contents = $body
  }
  if (obj.contents) {
    obj.contents = obj.contents.not(function() { return $(this).is('title') })
    obj.contents.find('title').remove()
    obj.scripts = findAll(obj.contents, 'script[src]').remove()
    obj.contents = obj.contents.not(obj.scripts)
  }
  if (obj.title) obj.title = $.trim(obj.title)
  return obj
}
function executeScriptTags(scripts) {
  if (!scripts) return
  var existingScripts = $('script[src]')
  scripts.each(function() {
    var src = this.src
    var matchedScripts = existingScripts.filter(function() {
      return this.src === src
    })
    if (matchedScripts.length) return
    var script = document.createElement('script')
    var type = $(this).attr('type')
    if (type) script.type = type
    script.src = $(this).attr('src')
    document.head.appendChild(script)
  })
}
var cacheMapping      = {}
var cacheForwardStack = []
var cacheBackStack    = []
function cachePush(id, value) {
  cacheMapping[id] = value
  cacheBackStack.push(id)
  trimCacheStack(cacheForwardStack, 0)
  trimCacheStack(cacheBackStack, pjax.defaults.maxCacheLength)
}
function cachePop(direction, id, value) {
  var pushStack, popStack
  cacheMapping[id] = value
  if (direction === 'forward') {
    pushStack = cacheBackStack
    popStack  = cacheForwardStack
  } else {
    pushStack = cacheForwardStack
    popStack  = cacheBackStack
  }
  pushStack.push(id)
  id = popStack.pop()
  if (id) delete cacheMapping[id]
  trimCacheStack(pushStack, pjax.defaults.maxCacheLength)
}
function trimCacheStack(stack, length) {
  while (stack.length > length)
    delete cacheMapping[stack.shift()]
}
function findVersion() {
  return $('meta').filter(function() {
    var name = $(this).attr('http-equiv')
    return name && name.toUpperCase() === 'X-PJAX-VERSION'
  }).attr('content')
}
function enable() {
  $.fn.pjax = fnPjax
  $.pjax = pjax
  $.pjax.enable = $.noop
  $.pjax.disable = disable
  $.pjax.click = handleClick
  $.pjax.submit = handleSubmit
  $.pjax.reload = pjaxReload
  $.pjax.defaults = {
    timeout: 650,
    push: true,
    replace: false,
    type: 'GET',
    dataType: 'html',
    scrollTo: 0,
    maxCacheLength: 20,
    version: findVersion
  }
  $(window).on('popstate.pjax', onPjaxPopstate)
}
function disable() {
  $.fn.pjax = function() { return this }
  $.pjax = fallbackPjax
  $.pjax.enable = enable
  $.pjax.disable = $.noop
  $.pjax.click = $.noop
  $.pjax.submit = $.noop
  $.pjax.reload = function() { window.location.reload() }
  $(window).off('popstate.pjax', onPjaxPopstate)
}
if ($.event.props && $.inArray('state', $.event.props) < 0) {
  $.event.props.push('state')
} else if (!('state' in $.Event.prototype)) {
  $.event.addProp('state')
}
$.support.pjax =
  window.history && window.history.pushState && window.history.replaceState &&
  !navigator.userAgent.match(/((iPod|iPhone|iPad).+\bOS\s+[1-4]\D|WebApps\/.+CFNetwork)/)
if ($.support.pjax) {
  enable()
} else {
  disable()
}
})(jQuery)
$(function() {
    $(document).pjax('a[target!=_blank]:not(#pagination a,.lightgallery_item,.sign-function ul li a,.sign-exit a)', '#header_content', {fragment:'#header_content', timeout:8000});
	$(document).on("submit", ".s-form", "btnPost",
	function(a) {
		$.pjax.submit(a, "#header_content", {
			fragment: "#header_content",
			timeout: 6000
		})
	});
	$(document).on('pjax:send', function() {
		$(".pjax_loading").show();
	});
	$(document).on('pjax:complete', function() {
		Lucky_pjaxafter();
		try {if(typeof Prism === "function") {} else {prism_pjaxafter();};} catch(e) {}
		if ($('#geetest').length != 0){geetest();}
		$(".s-form .s-key").val("");
		$(".pjax_loading").hide();
	});
});
function geetest(){var gtplug={};(function(){zbp.cookie.set("geeteston",1,1);zbp.cookie.set("geetestchallenge","",1);zbp.cookie.set("geetestvalidate","",1);zbp.cookie.set("geetestseccode","",1);if($('#geetest').length==0)return;$.ajax({url:zbp.options.bloghost+"/zb_users/theme/Lucky/initialize.php?act=geetest&rnd="+(new Date()).getTime(),type:"get",dataType:"json",success:function(data){initGeetest({gt:data.gt,challenge:data.challenge,offline:!data.success,product:"float",new_captcha:data.new_captcha},function(captchaObj){captchaObj.appendTo('#geetest');if(captchaObj.reset){captchaObj.refresh=captchaObj.reset}zbp.plugin.on("comment.postsuccess","geetest",function(){captchaObj.refresh()});captchaObj.onSuccess(function(){var validate=captchaObj.getValidate();zbp.cookie.set("geetestchallenge",validate.geetest_challenge,1);zbp.cookie.set("geetestvalidate",validate.geetest_validate,1);zbp.cookie.set("geetestseccode",validate.geetest_seccode,1);zbp.cookie.set("geeteston",0,1);for(var key in gtplug){gtplug[key]()}})})}})})()}
function prism_pjaxafter(){var compatibility={as3:"actionscript","c#":"csharp",delphi:"pascal",html:"markup",xml:"markup",vb:"basic",js:"javascript",plain:"markdown",pl:"perl",ps:"powershell"};var runFunction=function(doms,callback){doms.each(function(index,unwrappedDom){var dom=$(unwrappedDom);var codeDom=$("<code>");if(callback)callback(dom);var languageClass="prism-language-"+function(classObject){if(classObject===null)return"markdown";var className=classObject[1];return compatibility[className]?compatibility[className]:className}(dom.attr("class").match(/prism-language-([0-9a-zA-Z]+)/));codeDom.html(dom.html()).addClass("prism-line-numbers").addClass(languageClass);dom.html("").addClass(languageClass).append(codeDom)})};runFunction($("pre.prism-highlight"));runFunction($('pre[class*="brush:"]'),function(preDom){var original;if((original=preDom.attr("class").match(/brush:([a-zA-Z0-9\#]+);/))!==null){preDom.get(0).className="prism-highlight prism-language-"+original[1]}});Prism.highlightAll()}
function Lucky_pjaxafter(){
	$(function() {
		/* lightgallery */
		$('#main').lightGallery({zoomFromImage: false,selector:'.lightgallery_item',thumbnail:true,animateThumb: false,showThumbByDefault: false});
		/* 导航高亮 */
		var datatype=$("#nav").attr("data-type");$("#css3_menu li").each(function(){try{var myid=$(this).attr("id");if("index"==datatype){if(myid=="navbar-item-index"){$("#navbar-item-index>a").addClass("current-menu-item")}}else if("category"==datatype){var infoid=$("#nav").attr("data-infoid");if(infoid!=null){var b=infoid.split(' ');for(var i=0;i<b.length;i++){if(myid=="navbar-category-"+b[i]){$("#navbar-category-"+b[i]+">a").addClass("current-menu-item")}}}var crootid=$("#nav").attr("data-rootid");if(crootid!=null){var ccateid=$("#nav").attr("data-rootid");$("#css3_menu li").each(function(){$("#navbar-category-"+ccateid+">a").addClass("current-menu-item")})}}else if("article"==datatype){var infoid=$("#nav").attr("data-infoid");if(infoid!=null){$("#css3_menu li").each(function(){$("#navbar-category-"+infoid+">a").addClass("current-menu-item")})}var arootid=$("#nav").attr("data-rootid");if(arootid!=null){var cateid=$("#nav").attr("data-rootid");$("#css3_menu li").each(function(){$("#navbar-category-"+cateid+">a").addClass("current-menu-item")})}}else if("page"==datatype){var infoid=$("#nav").attr("data-infoid");if(infoid!=null){if(myid=="navbar-page-"+infoid){$("#navbar-page-"+infoid+">a").addClass("current-menu-item")}}}else if("tag"==datatype){var infoid=$("#nav").attr("data-infoid");if(infoid!=null){if(myid=="navbar-tag-"+infoid){$("#navbar-tag-"+infoid+">a").addClass("current-menu-item")}}}}catch(E){}});
		/* 登录框 */
		$('.signin-loader').on('click',function(){$('body').addClass('sign-show');setTimeout(function(){$('#sign-in').show().find('input:first').focus()},300)});$('.sign-mask').on('click',function(){$('body').removeClass('sign-show')});
		/* 电脑版搜索 */
		$('.search-on-off').click(function() {$("#search").fadeIn(233);$('.search-bg').on('click',function(){$('#search').fadeOut(233);});});
		/* 手机导航高亮 */
		var s=document.location;$("#navs li a").each(function(){if(this.href==s.toString().split("#")[0]){$(this).addClass("on");return false}});
		/* 文章归档 */
		var $a=$('#archives'),$m=$('.al_mon',$a),$l=$('.al_post_list',$a),$l_f=$('.al_post_list:first',$a);$l.hide();$l_f.show();$m.css('cursor','pointer').on('click',function(){$(this).next().slideToggle(400)});var animate=function(index,status,s){if(index>$l.length){return}if(status=='up'){$l.eq(index).slideUp(s,function(){animate(index+1,status,(s-10<1)?0:s-10)})}else{$l.eq(index).slideDown(s,function(){animate(index+1,status,(s-10<1)?0:s-10)})}};$('#al_expand_collapse').on('click',function(e){e.preventDefault();if($(this).data('s')){$(this).data('s','');animate(0,'up',100)}else{$(this).data('s',1);animate(0,'down',100)}});
		/* 图片延迟加载 */
		$("img:not(#pay .qrcode-img)").lazyload({placeholder:upyunbloghost+"/zb_users/theme/Lucky/style/image/grey.gif",effect:"fadeIn",failurelimit:30});
		/* TAB */
		$("#tab").rTabs({bind:'hover',animation:'fadein'});
		/* Ajax搜索，即时显示搜索结果 */
		var sr=$('#search-result');var j=bloghost+"search.php";if(sr.length>=0){var i=("search.php");$("#search .s-key").on("input",function(b){i=b.timeStamp;var c=$(this).val().replace(/\s+/g," ");$('input[name="s"]').eq(0).attr('autocomplete','off');return" "==c?!1:(""!=c?setTimeout(function(){0==i-b.timeStamp&&$.ajax({type:"GET",url:j,data:"q="+c,beforeSend:function(){sr.empty().show().append('<li><a href="javascript:;" style="text-align:center">Loading...</a></li>')},success:function(b){var c=$(b).find('.post-title a').length,d=sr.data("reusltnum");if(""==d&&(d=c),num=d>=c?c:d,0==c)sr.empty().show().append('<li class="search-result-nopost"><a href="javascript:;">没有找到你要搜索的内容</a></li>');else{var r_href=new Array(),r_text=new Array();$(b).find('.post-title a').each(function(r){r_href[r]=$(this).attr('href');r_text[r]=$(this).text()});sr.empty().show();for(var e=0;num>e;e++){sr.append('<li><a href="'+r_href[e]+'">'+r_text[e]+"</a></li>");if(e==7){sr.append('<li class="search-result-nopost"><a href="javascript:;">回车查看更多 ...</a></li>')}}}}})},500):(sr.empty().hide()),void 0)}).blur(function(){var b=$(this).val().replace(/\s+/g," ");(""==b||" "==b)&&(sr.empty().hide())})}
		/* 随机图片 */
		now=new Date();randimg=now.getSeconds();randimg=randimg-parseInt(randimg/numnum)*numnum+1;LoadImage();function LoadImage(){var img_url=$("#img_url").val();if(img_url==''){img_url=""+upyunbloghost+"/zb_users/theme/Lucky/style/rand/bg"+randimg+".jpg"}var img=new Image();$(img).load(function(){$(this).hide();$('#img_holder').removeClass('loadit').append(img);$(img).fadeIn()}).attr('src',img_url).attr('id','current_img')};
		/* 直达底部和返回顶部和直达评论 */
    $('#j-top').on('click',function(){$('html,body').animate({'scrollTop':0},628)});$('.twitter_time span a,.info_comment a,#to_comment').on('click',function(){$('html,body').animate({scrollTop:$('#post-comment-list').offset().top},628)});$('#j-down').click(function(){$('html,body').stop();$('html,body').animate({scrollTop:$('#footer').offset().top},628)});
    /* 二维码 */
    $(".wechat").mouseover(function (){$('.wechat-wrap').show();}).mouseout(function (){$('.wechat-wrap').hide();});
    /* UBB相关 */
		UBB4ZBPinsertUbbFace();
		$('#faces').click(function(){$("#UbbFrame").slideToggle(666)});
	});
	/* pjax后关闭mmenu及清空手机版搜索框内容 */
	var API = $("nav#menu").data("mmenu");API.close();$("#nav-search").val("");
	/* 打赏和分享 */
	(function($){$(function(){var $pay=$("#pay").hide();$pay.find(".ds-payment-way").bind("click",function(){$pay.find(".qrcode-img").hide();$pay.find(".qrCode_"+$pay.find("input[name=reward-way]:checked").val()).show()});$pay.find(".ds-close-dialog").bind("click",function(){$pay.fadeOut(233)})});var PaymentUtils=window['PaymentUtils']={};PaymentUtils.show=function(){$("#pay").fadeIn(233)};PaymentUtils.hide=function(){$("#pay").fadeOut(233)};var Post_Share=window['Post_Share']={};Post_Share.show=function(){$("#post_share").fadeIn(233)};Post_Share.hide=function(){$("#post_share").fadeOut(233)};$("#post_share").find(".ds-close-dialog").bind("click",function(){$("#post_share").fadeOut(233)})})(jQuery);
	/* 兼容FY代码高亮 */
	if($.isFunction(window.prettyPrint)){jQuery("pre").addClass("prettyprint");prettyPrint()};
	/* 百度分享、锚点处理、多说 */
	try{history.pushState(null,"",href);if(href.indexOf("#")!=-1){var toid=href.substring(href.indexOf("#"));$("html,body").animate({scrollTop:$(toid).offset().top},1000)}else{$("html,body").animate({scrollTop:0},1000)}}catch(E){}try{if(DUOSHUO!=null){DUOSHUO.EmbedThread(".ds-thread")}}catch(E){}try{if(window._bd_share_main!=null){window._bd_share_main.init()}}catch(E){}
	/* 极验验证 */
	if ($.isFunction(window.gtfunc)){gtfunc();}
	/* Swiper幻灯片 */
	if ($.isFunction(window.Swiper)){var swiper = new Swiper('.swiper-container', {pagination: '.swiper-pagination',nextButton: '.swiper-button-next',prevButton: '.swiper-button-prev',paginationClickable: true,centeredSlides: true,autoplay: 3333,loop: true});}
	/* 侧栏跟随 */
	$(function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.flw"),f="object"==typeof b&&b;e||d.data("bs.flw",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.t=a(this.options.target).on("scroll.bs.flw.data-api",a.proxy(this.checkPosition,this)).on("click.bs.flw.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.e=a(b),this.flwed=this.unpin=this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.2.0",c.RESET="flw flw-top flw-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.e.removeClass(c.RESET).addClass("flw");var a=this.t.scrollTop(),b=this.e.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.e.is(":visible")){var b=a(document).height(),d=this.t.scrollTop(),e=this.e.offset(),f=this.options.offset,g=f.top,h=f.bottom;"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top(this.e)),"function"==typeof h&&(h=f.bottom(this.e));var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.e.height()>=b-h?"bottom":null!=g&&g>=d?"top":!1;if(this.flwed!==i){null!=this.unpin&&this.e.css("top","");var j="flw"+(i?"-"+i:""),k=a.Event(j+".bs.flw");this.e.trigger(k),k.isDefaultPrevented()||(this.flwed=i,this.unpin="bottom"==i?this.getPinnedOffset():null,this.e.removeClass(c.RESET).addClass(j).trigger(a.Event(j.replace("flw","flwed"))),"bottom"==i&&this.e.offset({top:b-this.e.height()-h}))}}};var d=a.fn.flw;a.fn.flw=b,a.fn.flw.Constructor=c,a.fn.flw.noConflict=function(){return a.fn.flw=d,this},a(window).on("load",function(){a('[data-spy="flw"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},d.offsetBottom&&(d.offset.bottom=d.offsetBottom),d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})});var e=$(_js.sID),w=$(_js.bID).height()+(+_js.sTP);if(e.length){var h=$(_js.fID);h.flw({offset:{top:e.offset().top+e.height(),bottom:w}});h.on("flw-top.bs.flw",function(){h.css({top:0})});h.on("flw.bs.flw",function(){h.css({top: +_js.sTP})})}});
}
//监听后退，然后执行某个东西；
jQuery(document).ready(function($) {if (window.history && window.history.pushState) {$(window).on('popstate', function() {var hashLocation = location.hash;var hashSplit = hashLocation.split("#!/");var hashName = hashSplit[1];if (hashName !== '') {var hash = window.location.hash;if (hash == '') {Lucky_pjaxafter();};};});};});